/* Karma configuration */
module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', 'requirejs'],
    files: [
      'test-main.js',
      { pattern: "node_modules/requirejs-text/text.js", included: false },
      { pattern: 'lib/**/*.js', included: false },
      { pattern: 'test/**/*.js', included: false },
      { pattern: 'test/**/*.json', included: false }
    ],
    preprocessors: {
      'lib/**/*.js': 'coverage'
    },
    reporters: ['progress', 'coverage', 'html'],
    htmlReporters: {
      outputDir: 'test_result'
    },
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['Chrome']
  });
};
