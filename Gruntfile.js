/*global module:false*/
var fs = require('fs');
var Mark = require('markup-js');

module.exports = function(grunt) {
    var WEEKS = [3, 4, 5, 8, 10, 13];

    var PARSER_FILES = WEEKS.map(function (week) {
        return 'lib/parser/parser-week-' + week + '.js';
    });

    var JISON_CONFIG = (function () {
        var config = {};
        WEEKS.forEach(function(week) {
            var outputJSName = 'lib/parser/parser-week-' + week + '.js';
            var jisonInputName = 'lib/parser/parser-week-'  + week + '.jison';
            var files = {};
            files[outputJSName] = jisonInputName;
            config["jediscript-week-" + week] = {
                options: {
                    moduleType: 'amd'
                },
                files: files
            };
        });
        return config;
    })();

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jison: JISON_CONFIG,
        requirejs: {
            compile: {
                options: {
                    almond: true,
                    baseUrl: '.',
                    include: ['node_modules/almond/almond', 'lib/main']
                        .concat(PARSER_FILES),
                    wrap: {
                        startFile: 'lib/start.frag',
                        endFile: 'lib/end.frag'
                    },
                    out: 'dist/jedi-runtime.js'
                }
            },
            jfdi: {
                options: {
                    almond: true,
                    baseUrl: 'jfdi',
                    optimize: 'none',
                    include: ['../node_modules/almond/almond', 'main',
                              '../dist/jedi-runtime'],
                    wrap: {
                        startFile: 'jfdi/start.frag',
                        endFile: 'jfdi/end.frag'
                    },
                    out: 'dist/jfdi-runtime.js'
                }
            }
        },
        copy: {
            parser: {
                nonull: true,
                src: 'lib/parser/parser-week-' + WEEKS[WEEKS.length - 1] + '.js',
                dest: 'lib/parser/parser.js'
            }
        },
        concat: {
          options: {
            separator: ';',
          },
          dist: {
            src: [
              'jfdi/lib/misc.js',
              'jfdi/lib/list.js',
              'jfdi/lib/object.js',
              'jfdi/lib/stream.js',
              'jfdi/lib/interpreter.js',
              'dist/jfdi-runtime.js'
            ],
            dest: 'dist/jfdi-runtime.js'
          },
        },
        jshint: {
            files: ['Gruntfile.js', 'lib/**/*.js', 'test/**/*.js'],
            options: {
                ignores: ['lib/parser/parser*']
            }
        },
        karma: {
            unit: {
                configFile: 'karma.conf.js',
                singleRun: true
            }
        },
        watch: {
          scripts: {
              files: ['lib/**/*.js', 'test/**/*.js'],
              tasks: ['jshint', 'requirejs']
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-jison');
    grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('default', ['jshint', 'parser', 'requirejs', 'concat']);
    grunt.registerTask('test', ['jshint', 'parser', 'karma:unit']);

    grunt.registerTask('parser', 'Builds the JediScript parser from the Jison template', function() {
        var TEMPLATE = 'lib/parser/parser.jison.tpl';

        function readFile(filename, encoding) {
            return fs.readFileSync(filename, encoding || "utf8");
        }

        function writeFile(filename, data) {
            fs.writeFileSync(filename, data);
        }

        function readTemplate(context) {
            var template = readFile(TEMPLATE);
            return Mark.up(template, context);
        }

        function generateParser(week) {
            // Build the Includes for the parser.
            var includes = '';
            grunt.file.recurse('lib/parser/includes', function (path) {
                if (!path.match(/\.js/i)) {
                    return;
                }

                includes += '/* ' + path + '*/\n';
                includes += readFile(path);
                includes += ';\n';
            });
            writeFile(
                'lib/parser/parser-week-' + week + '.jison',
                readTemplate({ includes: includes, week: week }));
        }
        WEEKS.forEach(generateParser);
        grunt.task.run('jison');
        grunt.task.run('copy:parser');
    });
};
