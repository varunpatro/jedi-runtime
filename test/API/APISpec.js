define(['lib/compiler/compiler', 'lib/parser/parser', 'lib/vm/runtime', 'lib/debug/debug'],
	function(compile, Parser, Runtime, Debug) {

	describe('Compiler', function() {
		it('should be able to generate required debug symbols', function(){
			var parse_tree = Parser.parse('var x = 1;');
			var compiledObject = compile(parse_tree);
			var instructions = compiledObject.instructions;
			var debug = compiledObject.debug;
			expect(instructions).not.toBeUndefined();
			expect(debug).not.toBeUndefined();
		});
	});
});
