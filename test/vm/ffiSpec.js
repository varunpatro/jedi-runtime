/*globals define, describe, it, beforeEach, window, expect, spyOn */
/** @module ffiSpec
 *  Test the functionality of the foreign function interface.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(['lib/vm/ffi',
        'lib/vm/instruction',
        'lib/vm/runtime'],
function(FFI, Instruction, Runtime) {
"use strict";
describe("FFI module", function() {
    var runtime;
    var imports;
    beforeEach(function() {
        window.testFun = function(x) {
            return x + 3;
        };
        window.testValue = 42;
        window.testLambda = function() {
            return function() {
                return 42;
            };
        };
        window.testUseLambda = function(f) {
            return f(2);
        };
        window.testUseObject = function(o) {
            o.x++;
            return o.foo(2);
        };
        window.Constructor = function(x, y) {
            this.x = x;
            this.y = y;
        };
        imports = ["testFun", "testValue", "testLambda",
                   "testUseLambda", "testUseObject", "Constructor"];
        runtime = new Runtime(imports);
    });
    it("is able to populate global environment", function() {
        expect(runtime.environment.get("testValue")).toBe(window.testValue);
        expect(runtime.environment.get("testFun").address).toBe(0);
        expect(runtime.environment.get("testFun").is_native).toBeTruthy();
    });
    it("is able to call native javascript function", function() {
        spyOn(FFI, "call").and.callThrough();
        // testFun(2);
        expect(runtime.execute_instruction([
            new Instruction("LDS", "testFun"),
            new Instruction("LDU"),
            new Instruction("LDCN", 2),
            new Instruction("CALL", 2),
            new Instruction("DONE")
        ]).value).toBe(5);
        expect(FFI.call).toHaveBeenCalledWith(0, [2], runtime.ffi_register);
    });
    it("is able to return native closure if the call returns lambda",
       function() {
        spyOn(FFI, "call").and.callThrough();
        // (testLambda())();
        expect(runtime.execute_instruction([
            new Instruction("LDS", "testLambda"),
            new Instruction("LDU"),
            new Instruction("CALL", 1),
            new Instruction("LDU"),
            new Instruction("CALL", 1),
            new Instruction("DONE")
        ]).value).toBe(42);
    });
    it("is able to wrap function type argument in separate execution context",
       function() {
        expect(runtime.execute_instruction([
            new Instruction("LDS", "x"),
            new Instruction("LDCN", 1),
            new Instruction("PLUS"),
            new Instruction("RTN"),
            new Instruction("LDS", "testUseLambda"),
            new Instruction("LDU"),
            new Instruction("LDF", [0, "this", "x"]),
            new Instruction("CALL", 2),
            new Instruction("DONE")
        ], 4).value).toBe(3);
    });
    it("is able to call a function inside an object passed as argument",
       function() {
        expect(runtime.execute_instruction([
            new Instruction("LDCN", 3),
            new Instruction("LDCS", "x"),
            new Instruction("LDF", [4, "this", "y"]),
            new Instruction("GOTOR", 5),
            // start function body
            new Instruction("LDS", "y"),
            new Instruction("LDS", "y"),
            new Instruction("TIMES"),
            new Instruction("RTN"),
            // end function body
            new Instruction("DECLARE", "obj"),
            new Instruction("LDCS", "foo"),
            new Instruction("LDCO", 2),
            new Instruction("STORE", "obj"),
            // end define obj
            new Instruction("LDS", "testUseObject"),
            new Instruction("LDU"),
            new Instruction("LDS", "obj"),
            new Instruction("CALL", 2),
            new Instruction("LDS", "obj"),
            new Instruction("LDCS", "x"),
            new Instruction("READPS"),
            new Instruction("DONE")
        ]).value).toBe(4);
    });
    it("is able to construct a foreign javascript object",
       function() {
           var obj = runtime.execute_instruction([
               new Instruction("LDS", "Constructor"),
               new Instruction("LDU"),
               new Instruction("LDCN", 3),
               new Instruction("LDCN", 4),
               new Instruction("CONSTRUCT", 3),
               new Instruction("DONE")]).value;
           console.log(obj);
           expect(obj instanceof Constructor).toBe(true);
           expect(obj.x).toBe(3);
           expect(obj.y).toBe(4);
    });
});
});
