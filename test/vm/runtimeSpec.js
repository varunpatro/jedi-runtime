/*globals define,describe,it */
/** @module runtimeSpec
 *  Test the functionality of the jediscript virtual machine.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(function(require) {
"use strict";

var literalExpression = require("text!test/vm/mock/unaryExpression.json");
var unaryExpression = require("text!test/vm/mock/unaryExpression.json");
var binaryExpression = require("text!test/vm/mock/binaryExpression.json");
var illTypedExpression = require("text!test/vm/mock/illTypedExpression.json");
var conditionals = require("text!test/vm/mock/conditionals.json");
var variables = require("text!test/vm/mock/variables.json");
var functions = require("text!test/vm/mock/functions.json");
var records = require("text!test/vm/mock/records.json");
var arrays = require("text!test/vm/mock/arrays.json");
var objectConstruction = require("text!test/vm/mock/objectConstruction.json");
var Driver = require("test/vm/driver");
var Runtime = require("lib/vm/runtime");
var Instruction = require("lib/vm/instruction");

describe("A runtime environment", function() {
    it("is able to detect if running inside web worker", function() {
        var runtime = new Runtime();
        expect(runtime.is_inside_web_worker).toBe(false);
    });
    it("is able to execute more instructions", function() {
        var instruction1 = [
          new Instruction("DECLARE", "x"),
          new Instruction("LDCN", 3),
          new Instruction("LDCN", 4),
          new Instruction("PLUS"),
          new Instruction("STORE", "x"),
          new Instruction("LDU"),
          new Instruction("DONE")
        ];
        var instruction2 = [
          new Instruction("LDS", "x")
        ];
        var runtime = new Runtime();
        runtime.execute_instruction(instruction1);
        expect(runtime.execute_more_instruction(instruction2).value).toBe(7);
    });
});

describe("A virtual machine that supports ePL", function() {
    it("is able to execute LDU", function() {
        var runtime = new Runtime();
        expect(runtime.execute_instruction([
            new Instruction("LDU"),
            new Instruction("DONE")]).value)
        .toBe(undefined);
    });
    it("is able to execute single literal expression", function() {
        var test = Driver.loadTest(literalExpression);
        Driver.runTest(test);
    });
    it ("is able to execute single unary expression", function() {
        var test = Driver.loadTest(unaryExpression);
        Driver.runTest(test);
    });
    it("is able to execute single n-ary expression", function() {
        var test = Driver.loadTest(binaryExpression);
        Driver.runTest(test);
    });
    /*
    it("is able to throw TypeError exceptions on ill-typed expressions", function() {
        var test = Driver.loadTest(illTypedExpression);
        Driver.runTestNegative(test);
    });
    */
    it("is able to throw InstructionNotFound literal expression", function() {
        var runtime = new Runtime();
        expect(function () {
            runtime.execute_instruction([ new Instruction("LOL") ]);
        }).toThrow();
    });
});

describe("simPL Virtual Machine", function() {
    it("is able to execute conditional statements", function() {
        var test = Driver.loadTest(conditionals);
        Driver.runTest(test);
    });
    it("is able to execute variable definition", function() {
        var test = Driver.loadTest(variables);
        Driver.runTest(test);
    });
    it("is able to execute function definition and application", function() {
        var test = Driver.loadTest(functions);
        Driver.runTest(test);
    });
});

describe("rePL Virtual Machine", function() {
    it("is able to execute records operations", function() {
        var test = Driver.loadTest(records);
        Driver.runTest(test);
    });
    it("is able to execute array operations", function() {
        var test = Driver.loadTest(arrays);
        Driver.runTest(test);
    });
    it("is able to execute object construction operations", function() {
        var test = Driver.loadTest(objectConstruction);
        Driver.runTest(test);
    });
});

});
