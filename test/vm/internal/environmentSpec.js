/*globals define, describe, beforeEach, it, expect */
/** @module environmentSpec
 *  Test the functionality of the environment model class.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */

define(['lib/vm/internal/environment'], function(Environment) {
"use strict";

var Closure = require('lib/vm/internal/closure');

describe("An environment ", function() {
    var env;
    var parent;

    beforeEach(function() {
        env = null;
        parent = null;
    });

    it("is able to add and find variable binding", function() {
        env = new Environment(null);
        env.add("x", 3);
        expect(env.get("x")).toBe(3);
    });
    it("is able to shadow variable from enclosing environment", function() {
        parent = new Environment(null);
        parent.add("x", 2);
        env = new Environment(parent);
        env.add("x", 3);
        expect(env.get("x")).toBe(3);
    });
    it("is able to find variable from enclosing environment", function() {
        parent = new Environment(null);
        env = new Environment(parent);
        parent.add("x", 3);
        expect(env.get("x")).toBe(3);
    });
    it("is able to mutate variable", function() {
        env = new Environment(null);
        env.add("x", 3);
        env.update("x", 4);
        expect(env.get("x")).toBe(4);
    });
    it("is able to mutate variable on enclosing environment", function() {
        parent = new Environment(null);
        parent.add("x", 2);
        env = new Environment(parent);
        env.update("x", 3);
        expect(env.get("x")).toBe(3);
    });
    it("is able to throw exception when accessing unbound variable", function() {
        parent = new Environment(null);
        env = new Environment(parent);
        expect(function() {
            env.get("waldo");
        }).toThrow();
    });
    it("is able to throw exception when accessing unbound variable", function() {
        env = new Environment(null);
        env.add("foo", { bar : { baz : 2 } });
        env.chain_update(["foo", "bar", "baz" ], 3);
        expect((env.get("foo")).bar.baz).toBe(3);
    });
    it("is able to identify if a variable is a Closure", function() {
        env = new Environment(null);
        env.add("closure", new Closure());
        env.add("notClosure", {});
        expect(Environment.is_closure(env.get("closure"))).toBe(true);
        expect(Environment.is_closure(env.get("notClosure"))).toBe(false);
    });
    it("is able to identify if a variable is an object", function() {
        env = new Environment(null);
        env.add("object", {});
        env.add("notObject", true);
        expect(Environment.is_object(env.get("object"))).toBe(true);
        expect(Environment.is_object(env.get("notObject"))).toBe(false);
    });
});

});


