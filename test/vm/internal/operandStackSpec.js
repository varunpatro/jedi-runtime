/*globals define, describe, beforeEach, it, expect */
/** @module operandStackSpec
 *  Test the functionality of the operand stack model class.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(['lib/vm/internal/operandStack'], function(OperandStack) {
"use strict";

describe("An operand stack", function() {
    var os;

    beforeEach(function() {
        os = new OperandStack();
    });

    it("is able to push and pop values", function() {
        os.push(2);
        os.push(1);
        expect(os.top()).toBe(1);
        expect(os.pop()).toBe(1);
        expect(os.pop()).toBe(2);
    });

    it("is able to access values via indices", function() {
        os.push(3);
        os.push(2);
        os.push(1);
        expect(os.at(0)).toBe(1);
        expect(os.at(1)).toBe(2);
        expect(os.at(2)).toBe(3);
    });

    it("is able to throw exception when popping from empty stack", function() {
        expect(function() {
            os.pop();
        }).toThrow();
    });

    it("is able to throw exception when accessing out of bounds index",
            function() {
        os.push(3);
        expect(function() {
            os.at(1);
        }).toThrow();
        expect(function() {
            os.at(-1);
        }).toThrow();
    });
});
});


