/*globals define, expect */
/** @module driver
 *  The driver to loads and executes mock instructionArrays.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define([
    'lib/vm/instruction',
    'lib/vm/runtime'],
function(Instruction, Runtime) {
"use strict";

/** @class TestCase
 *  Class for test cases.
 */
function TestCase(instructionArray, expectedResult) {
    this.instructionArray = instructionArray;
    this.expectedResult = expectedResult;
}

return {
    /**
     *  Load a string representation of a JSON file and parse it to list of
     *  test cases which can be run by the runTest function
     *  @param {string} content The content of the .json file in raw string.
     * @returns {object} List of test cases that can be run by runTest function.
     */
    loadTest: function(content) {
        var testCaseList = [];
        var testData = JSON.parse(content);
        testData.forEach(function(data) {
            var instructionArray = [];
            var expectedResult = data.expectedResult;
            data.instruction.forEach(function(instruction) {
                if (instruction.length === 2) {
                    instructionArray.push(new Instruction(instruction[0],
                                                          instruction[1]));
                } else {
                    instructionArray.push(new Instruction(instruction[0]));
                }
            });
            testCaseList.push(new TestCase(instructionArray, expectedResult));
        });
        return testCaseList;
    },
    /**
     * Run a positive test case list generated by loadTest.
     * @param {object} testCaseList list of TestCase objects.
     */
    runTest: function(testCaseList) {
        testCaseList.forEach(function(testCase) {
            var runtime = new Runtime();
            var result = runtime.execute_instruction(testCase.instructionArray);
            expect(result.value).toBe(testCase.expectedResult);
        });
    },
    /**
     * Run a negative test case list generated by loadTest.
     * @param {object} testCaseList list of TestCase objects.
     */
    runTestNegative: function(testCaseList) {
        testCaseList.forEach(function(testCase) {
            var runtime = new Runtime();
            expect(function() {
                runtime.execute_instruction(testCase.instructionArray);
            }).toThrow();
        });
    }
};
});
