/*globals define, describe, it, expect */
/** @module instructionSpec
 *  Test the functionality of the instruction class.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(['lib/vm/instruction'], function(Instruction) {
    describe("Instruction module", function() {
        it("is able to construct instruction objects", function() {
            var testInstruction = new Instruction("LDCN", 10.0);
            expect(testInstruction instanceof Instruction).toBe(true);
            expect(testInstruction.name).toBe("LDCN");
            expect(testInstruction.value).toBe(10.0);
        });
    });
});
