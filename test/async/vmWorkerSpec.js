/*globals define, describe, it, beforeEach, window, expect, spyOn */
/** @module vmWorkerSpec
 *  Test the functionality of the VM Inside web worker.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(function(require) {
"use strict";

describe("VM Web Worker", function() {

var Thread = require('lib/messages');
var Msg = require('lib/messageLibrary');
var FFI = require('lib/vm/ffi');
var Instruction = require('lib/vm/instruction');

var DELAY_TIME = 100;
var vm_worker;

beforeEach(function(done) {
    setTimeout(function() {
        vm_worker = new Worker('/base/lib/vm/vm.js');
        Thread.subscribe(vm_worker)
        .when(Msg.VM.Response.MODULE_LOADED, function() {
            Thread.send_message(vm_worker, Msg.VM.Request.CREATE_INSTANCE,
                                "/base/test/async/import.js", ["internal"]);
            done();
        });
        function onError(e) {
          console.log('ERROR: Line ' + e.lineno + ' in ' + e.filename + ': ' + e.message);
        }
        vm_worker.addEventListener('error', onError, false);
    }, DELAY_TIME);
    window.external = function () {
        document.body.style.backgroundColor = "blue";
        return 3;
    };
});

it("is able to create instance", function(done) {
    Thread.subscribe(vm_worker)
    .when(Msg.VM.Response.INSTANCE_CREATED, function(vm_instance) {
        expect(vm_instance).toBeDefined();
        done();
    });
});

it("is web worker aware", function(done) {
    Thread.subscribe(vm_worker)
    .when(Msg.VM.Response.INSTANCE_CREATED, function(vm_instance) {
        expect(vm_instance.is_inside_web_worker).toBe(true);
        done();
    });
});
/*
it("is able to differentiate internal vs external FFI calls", function(done) {
    var ffi_register = [];
    try {
        Thread.subscribe(vm_worker)
        .when(Msg.VM.Response.INSTANCE_CREATED, function(vm_instance) {
            ffi_register = FFI.import_dom_modifying_function(vm_instance, ["external"]);
            expect(ffi_register.length).toBe(1);
            Thread.send_message(vm_worker, Msg.VM.Request.SET_INSTANCE, vm_instance);
        })
        .when(Msg.VM.Response.INSTANCE_SET, function(result) {
            var instructions = [
                new Instruction("LDS", "internal"),
                new Instruction("LDU"),
                new Instruction("CALL", 1),
                new Instruction("LDS", "external"),
                new Instruction("LDU"),
                new Instruction("CALL", 1),
                new Instruction("PLUS"),
                new Instruction("DONE")
            ];
            Thread.send_message(vm_worker, Msg.VM.Request.EXECUTE_INSTRUCTION,
                                instructions);
        })
        .when(Msg.VM.Response.EXECUTION_FINISHED, function(result) {
            expect(result).toBe(5);
            expect(document.body.style.backgroundColor).toBe("blue");
            done();
        })
        .when(Msg.Main.Request.FFI_CALL, function(ffi_request) {
            var ffi_result = FFI.call(ffi_request.address, ffi_request.args,
                                    ffi_register, true);
            Thread.send_message(vm_worker, Msg.Main.Response.FFI_CALL_DONE,
                                ffi_result);
        });
    } catch (e) {
        throw e.message;
    }
});
*/
it("is able to suspend and resume by supplying breakpoints", function(done) {
    var ffi_register = [];
    Thread.subscribe(vm_worker)
    .when(Msg.VM.Response.INSTANCE_CREATED, function(vm_instance) {
        var instructions = [
            new Instruction("LDCN", 1),
            new Instruction("LDCN", 2),
            new Instruction("PLUS"),
            new Instruction("DONE")
        ];
        Thread.send_message(vm_worker, Msg.VM.Request.EXECUTE_INSTRUCTION,
                            instructions, 0, { 1 : true });
    })
    .when(Msg.VM.Response.EXECUTION_SUSPENDED, function() {
        Thread.send_message(vm_worker, Msg.VM.Request.GET_RUNTIME_INFORMATION);
    })
    .when(Msg.VM.Response.RUNTIME_INFORMATION, function(info) {
        expect(info.pc).toBe(1);
        expect(info.operand_stack[0]).toBe(1);
        expect(info.runtime_stack.length).toBe(0);
        Thread.send_message(vm_worker, Msg.VM.Request.RESUME_EXECUTION);
    })
    .when(Msg.VM.Response.EXECUTION_FINISHED, function(result) {
        expect(result).toBe(3);
        done();
    });
});
});
});
