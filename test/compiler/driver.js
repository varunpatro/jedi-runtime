define(['lib/compiler/compiler', 'lib/parser/parser', 'lib/vm/runtime'], function(compile, Parser, Runtime){
'use strict';

var defaultImport = ["Math", "String"];

var assertFunctions = {
	'result_value': function(insts, test_entry) {
		return function(test_index) {
					var runtime = new Runtime(defaultImport);
					var result;
					expect(function(){result = runtime.execute_instruction(insts);})
						.not.toThrow();
					expect(result.value)
						.toBe(
							test_entry.expected,
							'Result value does not match' +
							' for test case No. ' +
							test_index);
				};
	},
	'instructions': function(insts, test_entry) {
		return function(test_index) {
					var expected_instructions = test_entry.expected;
					expect(insts.length).toBe(expected_instructions.length);
					insts.forEach(function(instruction, index) {
						expect(instruction.name)
							.toBe(expected_instructions[index][0],
								'Wrong instruction name at index ' +
								index +
								' for test case No. ' +
								test_index);
						expect(instruction.value)
							.toEqual(expected_instructions[index][1],
								'Wrong instruction parameter at index ' +
								index +
								' for test case No. ' +
								test_index);
					});
				};
	},
	'no_throw': function(insts, test_entry) {
		return function() {
					expect(function() {
						var runtime = new Runtime(defaultImport);
						runtime.execute_instruction(insts);
					}).not.toThrow();
				};
	},
	'throw': function(insts, test_entry) {
		return function() {
					expect(function() {
						var runtime = new Runtime(defaultImport);
						runtime.execute_instruction(insts);
					}).toThrow();
				};
	},
};

return {
	load_test: function(test_desc) {
		var test_data = JSON.parse(test_desc);
		return test_data.map(function(test_entry){
			var assert;
			var source = '';
			for(var i = 0, end = test_entry.source.length; i < end; ++i)
				source += test_entry.source[i] + '\n';
			var insts = compile(Parser.parse(source), source).instructions;

			assert = assertFunctions[test_entry.compare_mode](insts, test_entry);

			return assert;
		});
	},
	run_test_cases: function(test_cases) {
		for(var i = 0, end = test_cases.length; i < end; ++i) {
			test_cases[i](i);
		}
	}
};
});
