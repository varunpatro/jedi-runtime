define(['lib/compiler/compiler',
		'lib/parser/parser',
		'test/compiler/driver',
		'require',
		'text!test/compiler/mock/testVariable.json',
		'text!test/compiler/mock/testFunction.json',
		'text!test/compiler/mock/testControl.json',
		'text!test/compiler/mock/testTailcall.json',
		'text!test/compiler/mock/testRecord.json',
		'text!test/compiler/mock/testArray.json'],
	function(compile, Parser, Driver, require,
		testVariable,
		testFunction,
		testControl,
		testTailcall,
		testRecord,
		testArray){

	describe('compiler', function() {
		it('should compile loading and storing variables', function(){
			var test_cases = Driver.load_test(testVariable);
			Driver.run_test_cases(test_cases);
		});
		it('should compile function declaration and invocation', function(){
			var test_cases = Driver.load_test(testFunction);
			Driver.run_test_cases(test_cases);
		});
		it('should compile control flows', function() {
			var test_cases = Driver.load_test(testControl);
			Driver.run_test_cases(test_cases);
		});
		it('should recognise tail call patterns', function() {
			var test_cases = Driver.load_test(testTailcall);
			Driver.run_test_cases(test_cases);
		});
		it('should recognise record operations', function() {
			var test_cases = Driver.load_test(testRecord);
			Driver.run_test_cases(test_cases);
		});
		it('should recognise array declarations', function() {
			var test_cases = Driver.load_test(testArray);
			Driver.run_test_cases(test_cases);
		});
	});
});
