define(['test/compiler/driver',
		'text!test/compiler/units/testStatement.json',
		'text!test/compiler/units/testVariable.json',
		'text!test/compiler/units/testIf.json',
		'text!test/compiler/units/testWhile.json',

		'text!test/compiler/units/demoLambda.json',
		'text!test/compiler/units/demoRSA.json',
		'text!test/compiler/units/demoBank.json'
		],
function(Driver,
testStatement,
testVariable,
testIf,
testWhile,

demoLambda,
demoRSA,
demoBank

){
	// no test until parser is imported

	// OR we feed in parse trees manually

	// variable store and load
	describe('Compiler - subcompilers', function(){
		it('is able to compile sequences', function(){
			var test_cases = Driver.load_test(testStatement);
			Driver.run_test_cases(test_cases);
		});
		it('is able to compile variable definitions', function(){
			var test_cases = Driver.load_test(testVariable);
			Driver.run_test_cases(test_cases);
		});
		it('is able to compile if statements', function(){
			var test_cases = Driver.load_test(testIf);
			Driver.run_test_cases(test_cases);
		});
		it('is able to compile while statements', function(){
			var test_cases = Driver.load_test(testWhile);
			Driver.run_test_cases(test_cases);
		});
	});
	describe('CS1101S - demo', function() {
		it('calculates lambda calculus', function() {
			var test_cases = Driver.load_test(demoLambda);
			Driver.run_test_cases(test_cases);
		});
		it('runs number theory demostrations', function() {
			var test_cases = Driver.load_test(demoRSA);
			Driver.run_test_cases(test_cases);
		});
		it('runs Bank demo', function() {
			var test_cases = Driver.load_test(demoBank);
			Driver.run_test_cases(test_cases);
		});
	});
});