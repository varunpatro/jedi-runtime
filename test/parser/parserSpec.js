/* globals define, describe, it, expect */
/** @module parserSpec
 *  Test the behaviour of the parser.
 *  @author Joel Low <joel@joelsplace.sg>
 */
define(['lib/parser/parser'], function(Parser) {
    describe('Parser', function() {
        it("has a parse method", function() {
            expect(typeof Parser.parse).toBe('function');
        });

        describe('Lexer', function() {
            it("lexes away comments", function() {
                expect(Parser.parse('/* Nothing to see here */')).toEqual([]);
                expect(Parser.parse('// Nothing to see here')).toEqual([]);
            });
            it("lexes string escapes", function () {
                expect(Parser.parse('"";')[0]).toBe('');
                expect(Parser.parse('"\\n";')[0]).toBe('\n');
                expect(Parser.parse('"abc\\n123";')[0]).toBe('abc\n123');
            });
            it("does not strip comments within strings", function() {
                expect(Parser.parse('"hello"; // Nothing to see here')).toEqual(['hello', []]);
                expect(Parser.parse('"hello // Nothing to see here";')).toEqual(['hello // Nothing to see here', []]);
            });
        });

        describe("Parser", function() {
            describe("Line numbers", function() {
                it("correct line: function definition", function() {
                    expect(Parser.parse('function x() {}')[0].value).toEqual(
                        {
                            "tag": "function_definition",
                            "name": "x",
                            "parameters": [],
                            "body": [],
                            "location": {
                                "start_line": 1,
                                "start_col": 0,
                                "start_offset": 0,
                                "end_line": 1,
                                "end_col": 15,
                                "end_offset": 15
                            },
                            "line": 0
                        }
                    );
                });
            });

            it("ignores empty statement", function() {
                expect(Parser.parse(';')).toEqual([]);
                expect(Parser.parse('')).toEqual([]);
                expect(Parser.parse('0; ;')).toEqual([0, []]);
            });
        });
    });
});
