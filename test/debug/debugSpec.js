/** @module debugSpec
 *  Test the functionality of the jediscript debugger.
 *  @author Varun Patro <varun.kumar.patro@gmail.com>
 */
define(function(require) {

    // load test case list files
    var instructions = require("text!test/debug/mock/instructions.json");
    var apiTest = require("text!test/debug/mock/apiTest.json");
    var singleScope = require("text!test/debug/mock/singleScopeVariables.json");
    var multiScope = require("text!test/debug/mock/multiScopeVariables.json");
    // var breakpoints = require("text!test/debug/mock/breakpoints.json");
    var jstest = require("test/debug/mock/breakpoints");
    var loop = require("text!test/debug/mock/loop.json");

    // load source files
    var emptyFile = require("text!test/debug/mock/largeSourceText/emptyFile.js");
    var breakpoints_1 = require("text!test/debug/mock/largeSourceText/breakpoints_1.js");
    var threeNestedFunctions = require("text!test/debug/mock/largeSourceText/threeNestedFunctions.js");
    var twoNestedFunctions = require("text!test/debug/mock/largeSourceText/twoNestedFunctions.js");
    var fourFunctionCall = require("text!test/debug/mock/largeSourceText/fourFunctionCall.js");
    var fiveFunctionCall = require("text!test/debug/mock/largeSourceText/fiveFunctionCall.js");
    var whileLoopBinarySearch = require("text!test/debug/mock/largeSourceText/whileLoopBinarySearch.js");
    var forLoop = require("text!test/debug/mock/largeSourceText/forLoop.js");
    
    var Driver = require("test/debug/driver");

    describe('Debugger', function() {
        it('can make use of other APIs correctly.', function() {
            var test = Driver.loadSimpleTest(apiTest);
            Driver.runSimpleTest(test);
        });
        it('is able to obtain simple scope variables', function() {
            var test = Driver.loadSimpleTest(singleScope);
            Driver.runSimpleTest(test);
        });
        // it('is able to pause and resume multi-line source code', function() {
        //     var test = Driver.loadComplexTest(breakpoints);
        //     Driver.runComplexTest(test);
        // });
        it('is able to obtain nested scope variables', function() {
            var test = Driver.loadComplexTest(multiScope);
            Driver.runComplexTest(test);
        });
        // it('is able to read js file content', function() {
            // console.log(jstest);
        // });
        it('is able to run loops correctly', function() {
            var test = Driver.loadComplexTest(loop);
            Driver.runComplexTest(test);
        });
    });
});
