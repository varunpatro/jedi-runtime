define(function(require) {
    return [{
        "fileName": "breakpoints_1.js",
        "breakpointsArray": [2, 4],
        "expectedIntermediateResults": [{
            "variables": {
                "apple": "red",
                "banana": undefined,
                "carrot": undefined
            },
            "parent": null
        }, {
            "variables": {
                "apple": "red",
                "banana": "yellow",
                "carrot": undefined
            },
            "parent": null
        }],
        "expectedFinalResult": {
            "variables": {
                "apple": "red",
                "banana": "yellow",
                "carrot": "orange"
            },
            "parent": null
        }
    }];
});