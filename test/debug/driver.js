/** @module driver
 *  The driver to loads and executes mock source codes for the debugger.
 *  @author Varun Patro <varun.kumar.patro@gmail.com>
 */

define(function(require) {

    var Debug = require('lib/debug/debug');
    var Msg = require('lib/messageLibrary');

    /**
     * Simple test case class for basic testing
     * @param {string} code           the raw source code
     * @param expectedScopeVariables  the final scope variables
     */
    function SimpleTestCase(code, expectedScopeVariables) {
        this.code = code;
        this.expectedScopeVariables = expectedScopeVariables;
    }

    /**
     * Complex test case for breakpoints and testing intermediate results
     * @param {string} code                        the raw source code
     * @param {array} breakpointsArray             array of line number breakpoints
     * @param {array} expectedIntermediateResults  array of variable scopes
     * @param {object} expectedFinalResult         final variable scope
     */
    function ComplexTestCase(code, breakpointsArray, expectedIntermediateResults, expectedCallStack, expectedFinalResult) {
        this.code = code;
        this.breakpointsArray = breakpointsArray;
        this.expectedIntermediateResults = expectedIntermediateResults;
        this.expectedCallStack = expectedCallStack;
        this.expectedFinalResult = expectedFinalResult;
    }

    function loadSimpleTest(content) {
        var testCaseList = [];
        var testData = JSON.parse(content);
        testData.forEach(function(data) {
            testCaseList.push(new SimpleTestCase(data.code, data.expectedScopeVariables));
        });
        return testCaseList;
    }

    function runSimpleTest(testCaseList) {
        testCaseList.forEach(function(testCase) {
            var instance = new Debug(testCase.code);
            instance.start();
            var scopeVariables = instance.getScopeVariables();
            expect(scopeVariables).toEqual(testCase.expectedScopeVariables);
        });
    }

    function loadComplexTest(content) {
        var testCaseList = [];
        var testData = JSON.parse(content);
        testData.forEach(function(data) {
            code = require("text!test/debug/mock/largeSourceText/" + data.fileName);
            testCaseList.push(new ComplexTestCase(code, data.breakpointsArray, data.expectedIntermediateResults, data.expectedCallStack, data.expectedFinalResult));
        });
        return testCaseList;
    }

    function runComplexTest(testCaseList) {
        testCaseList.forEach(function(testCase) {
            var instance = new Debug(testCase.code);
            instance.setBreakpoints(testCase.breakpointsArray);
            var result = instance.start();
            while (result.status === Msg.VM.Response.EXECUTION_SUSPENDED) {
                var scopeVariables = instance.getScopeVariables();
                var callStack = instance.getCallStack();
                var expectedScopeVariables = testCase.expectedIntermediateResults.shift();
                var expectedCallStack = testCase.expectedCallStack.shift();
                expect(callStack.shift()).toBeUndefined();
                expect(callStack).toEqual(expectedCallStack);
                expect(scopeVariables).toEqual(expectedScopeVariables);
                result = instance.start();
            }
            var finalScopeVariables = instance.getScopeVariables();
            var expectedFinalScopeVariables = testCase.expectedFinalResult;
            expect(finalScopeVariables).toEqual(expectedFinalScopeVariables);
        });
    }

    return {
        loadSimpleTest: loadSimpleTest,
        runSimpleTest: runSimpleTest,
        loadComplexTest: loadComplexTest,
        runComplexTest: runComplexTest
    };

});