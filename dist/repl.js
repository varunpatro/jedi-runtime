var Repl = require('repl');
require("./jfdi-runtime");

var week_number = process.argv[2] ? parseInt(process.argv[2], 10) : 13;
JFDIRuntime.initialize(week_number);

const INITIAL_PROMPT = 'jedi> ';
const CONTINUATION_PROMPT = '  ... ';

var repl = Repl.start({
	prompt: INITIAL_PROMPT,
	eval: function(cmd, context, filename, callback) {
		return this.process(cmd, callback);
	},
	writer: JFDIRuntime.stringify
});

repl.evaluate = function(cmd, reset_environment) {
	return JFDIRuntime.parse_and_evaluate(cmd, reset_environment);
};

repl.reset = function() {
	this.evaluate('', true);
};

repl.break = function() {
	this.bufferedCommand = undefined;
};

repl.completer = function(text, callback) {
	callback([]);
};

repl.process = function(cmd, callback) {
	try {
		var result = this.evaluate(cmd, false);
		this.break();
		callback(null, result);
	} catch (e) {
		var message = typeof e === 'object' ? e.message : e;
		if (message.indexOf("'EOF'") !== -1) {
			this.bufferedCommand = cmd;
			this.displayPrompt();
		} else {
			throw e;
		}
	}
};

repl.displayPrompt = function(preserveCursor) {
	this._prompt = this.bufferedCommand ? CONTINUATION_PROMPT : INITIAL_PROMPT;
	return this.__proto__.displayPrompt.call(this, preserveCursor);
};

repl.on('reset', function(context) {
	this.reset();
});
