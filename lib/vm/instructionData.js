/*globals define */
/** @module instructionData
 *  Stores the metadata for VM Instructions.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(function(require) {
"use strict";

var Msg = require('lib/messageLibrary');
var FFI = require('lib/vm/ffi');
var Exception = require('lib/vm/exception');
var Environment = require('lib/vm/internal/environment');
var Closure = require('lib/vm/internal/closure');
var StackFrame = require('lib/vm/internal/stackFrame');
var OperandStack = require('lib/vm/internal/operandStack');
var Instruction = require('lib/vm/instruction');

var STACK_SIZE = 65536;

var Type = {
    NUMBER      : 'number',
    STRING      : 'string',
    BOOLEAN     : 'boolean',
    FUNCTION    : 'function',
    OBJECT      : 'object'
};

function getType(operand) {
  if(operand instanceof Closure) {
    return 'function';
  } else {
    return typeof operand;
  }
}

function generic_call(vm, argc, is_tail_call) {
    var closure = vm.operand_stack.at(argc);
    var popped = 0, arg, param;
    var is_foreign_object_method = !(closure instanceof Closure);
    // When wrapped method is get, use the original function.
    if (is_foreign_object_method &&
        closure.__as_closure__ instanceof Closure) {
        closure = closure.__as_closure__;
        is_foreign_object_method = false;
    }
    if (!is_foreign_object_method && (!closure.is_native)) {
        if (argc != closure.formals.length) {
          throw new Exception.WrongNumberOfArguments(argc,
            closure.formals.length);
        }
        var new_env = new Environment(closure.environment);
        while (popped < argc) {
            arg = vm.operand_stack.pop();
            param = closure.formals[argc - popped - 1];
            new_env.add(param, arg);
            popped++;
        }
        vm.operand_stack.pop();
        if (!is_tail_call) {
            if (vm.runtime_stack.length > STACK_SIZE) {
              vm.runtime_stack = [];
              throw new Exception.MaximumCallStackSizeExceeded();
            } else {
              vm.runtime_stack.push(new StackFrame(vm.pc, vm.environment,
                                                    vm.operand_stack));
            }
        }
        vm.pc = closure.address;
        vm.operand_stack = new OperandStack();
        vm.environment = new_env;
    } else {
        var args = [];
        while (popped < argc) {
            args.push(vm.operand_stack.pop());
            popped++;
        }
        args = args.reverse().map(function(a) {
            return FFI.vm_value_to_javascript(vm, a);
        });

        if (closure.is_external && vm.is_inside_web_worker) {
            return vm.suspend_for_ffi(closure.address, args);
        } else {
            var ffi_result;
            if (is_foreign_object_method) {
                ffi_result = FFI.call_wo_register(closure, args,
                                                    vm.ffi_register);
            } else {
                ffi_result = FFI.call(closure.address, args,
                                        vm.ffi_register);
            }
            vm.operand_stack.pop();
            vm.operand_stack.push(ffi_result);
            vm.pc++;
        }
    }
}
return {
    LDU: {
        param: 0,
        execute: function(vm) {
            vm.operand_stack.push(undefined);
            vm.pc++;
        }
    },
    LDCN: {
        param: 1,
        execute: function(vm, value) {
            vm.operand_stack.push(value);
            vm.pc++;
        }
    },
    LDCS: {
        param: 1,
        execute: function(vm, value) {
            vm.operand_stack.push(value);
            vm.pc++;
        }
    },
    LDCB: {
        param: 1,
        execute: function(vm, value) {
            vm.operand_stack.push(value);
            vm.pc++;
        }
    },
    PLUS: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER &&
                getType(operand1) !== Type.STRING &&
                getType(operand2) !== Type.NUMBER &&
                getType(operand2) !== Type.STRING) {
                throw new Exception.TypeError(Type.NUMBER + " or " +
                                              Type.STRING, getType(operand1),
                                              "left");
            }
            vm.operand_stack.push(operand1 + operand2);
            vm.pc++;
        }
    },
    SUB: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand1),
                  "left");
            } else if(getType(operand2) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand2),
                  "right");
            }
            vm.operand_stack.push(operand1 - operand2);
            vm.pc++;
        }
    },
    TIMES: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand1),
                  "left");
            } else if(getType(operand2) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand2),
                  "right");
            }
            vm.operand_stack.push(operand1 * operand2);
            vm.pc++;
        }
    },
    DIV: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand1),
                  "left");
            } else if(getType(operand2) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand2),
                  "right");
            }
            vm.operand_stack.push(operand1 / operand2);
            vm.pc++;
        }
    },
    MOD: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand1),
                 "left");
            } else if(getType(operand2) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand2),
                 "right");
            }
            vm.operand_stack.push(operand1 % operand2);
            vm.pc++;
        }
    },
    EQUAL: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== getType(operand2)) {
                throw new Exception.TypeError(getType(operand1), getType(operand2),
                  "right");
            }
            vm.operand_stack.push(operand1 === operand2);
            vm.pc++;
        }
    },
    NOTEQUAL: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== getType(operand2)) {
                throw new Exception.TypeError(getType(operand1), getType(operand2),
                  "right");
            }
            vm.operand_stack.push(operand1 !== operand2);
            vm.pc++;
        }
    },
    ANEG: {
        param: 0,
        execute: function(vm) {
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand1));
            }
            vm.operand_stack.push(-operand1);
            vm.pc++;
        }
    },
    BNEG: {
        param: 0,
        execute: function(vm) {
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.BOOLEAN) {
                throw new Exception.TypeError(Type.BOOLEAN, getType(operand1));
            }
            vm.operand_stack.push(!operand1);
            vm.pc++;
        }
    },
    LESSTHAN: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand1),
                  "left");
            } else if(getType(operand2) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand2),
                  "right");
            }
            vm.operand_stack.push(operand1 < operand2);
            vm.pc++;
        }
    },
    LESSTHANEQ: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand1),
                  "left");
            } else if(getType(operand2) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand2),
                  "right");
            }
            vm.operand_stack.push(operand1 <= operand2);
            vm.pc++;
        }
    },
    GREATERTHAN: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand1),
                  "left");
            } else if(getType(operand2) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand2),
                  "right");
            }
            vm.operand_stack.push(operand1 > operand2);
            vm.pc++;
        }
    },
    GREATERTHANEQ: {
        param: 0,
        execute: function(vm) {
            var operand2 = vm.operand_stack.pop();
            var operand1 = vm.operand_stack.pop();
            if (getType(operand1) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand1),
                  "left");
            } else if(getType(operand2) !== Type.NUMBER) {
                throw new Exception.TypeError(Type.NUMBER, getType(operand2),
                  "right");
            }
            vm.operand_stack.push(operand1 >= operand2);
            vm.pc++;
        }
    },
    JOFR: {
        param: 1,
        execute: function(vm, value) {
            var cond = vm.operand_stack.pop();
            if (getType(cond) !== Type.BOOLEAN) {
              throw new Exception.TypeError(Type.BOOLEAN, getType(cond));
            }
            vm.pc = cond? vm.pc + 1 : vm.pc + value;
        }
    },
    GOTOR: {
        param: 1,
        execute: function(vm, value) {
            vm.pc = vm.pc + value;
        }
    },
    LDS: {
        param: 1,
        execute: function(vm, variable_name) {
            vm.operand_stack.push(vm.environment.get(variable_name));
            vm.pc++;
        }
    },
    DECLARE: {
        param: 1,
        execute: function(vm, variable_name) {
            // Redeclaration of closure formals is not allowed.
            var parent = vm.environment.parent;
            vm.environment.add(variable_name, undefined);
            vm.pc++;
        }
    },
    LDF: {
        param: 1,
        execute: function(vm, declarator, source) {
            var address = declarator[0];
            var formals = declarator.slice(1);
            var template_env = new Environment(vm.environment);
            template_env.is_closure_formals = true;
            formals.forEach(function(param) {
                template_env.add(param, undefined);
            });
            var closure = new Closure(address, formals, template_env, source);
            closure.mock = FFI.wrap_to_vm_call(vm, closure);
            vm.operand_stack.push(closure);
            vm.pc++;
        }
    },
    CALL: {
        param: 1,
        execute: function(vm, argc) {
            return generic_call(vm, argc, false);
        }
    },
    TAILCALL: {
        param: 1,
        execute: function(vm, argc) {
            return generic_call(vm, argc, true);
        }
    },
    CONSTRUCT: {
        param: 1,
        execute: function(vm, argc) {
            var closure = vm.operand_stack.at(argc);
            var popped = 0, arg, param;
            var obj = {};
            if (closure instanceof Closure && !closure.is_native) {
                popped = 0;
                var new_env = new Environment(closure.environment);
                while (popped < argc) {
                    arg = vm.operand_stack.pop();
                    param = closure.formals[argc - popped - 1];
                    new_env.add(param, arg);
                    popped++;
                }
                vm.operand_stack.pop();
                vm.runtime_stack.push(new StackFrame(vm.pc, vm.environment,
                                                vm.operand_stack, true));
                obj.__proto__ = closure.mock.prototype; // jshint ignore:line
                new_env.update('this', obj);
                vm.pc = closure.address;
                vm.operand_stack = new OperandStack();
                vm.environment = new_env;
            } else {
                var args = [];
                while (popped < argc) {
                    args.push(vm.operand_stack.pop());
                    popped++;
                }
                args = args.reverse().map(function(a) {
                    return FFI.vm_value_to_javascript(vm, a);
                });
                obj = FFI.construct_foreign_object(closure, args,
                                                       vm.ffi_register);
                vm.operand_stack.pop();
                vm.operand_stack.push(obj);
                vm.pc++;
            }
        }
    },
    RTN: {
        param: 0,
        execute: function(vm) {
            var return_value = vm.operand_stack.pop();
            var restored_frame = vm.runtime_stack.pop();
            if (restored_frame.is_object_construction &&
                    return_value === undefined) {
                return_value = vm.environment.get('this');
            }
            vm.pc = restored_frame.pc;
            vm.environment = restored_frame.environment;
            vm.operand_stack = restored_frame.operand_stack;
            vm.operand_stack.push(return_value);
            vm.pc++;
        }
    },
    POP: {
        param: 0,
        execute: function(vm) {
            vm.operand_stack.pop();
            vm.pc++;
        }
    },
    LDCO: {
        param: 1,
        execute: function(vm, n) {
            var obj = {};
            var i, key, exp;
            for (i = 1; i <= n; i++) {
                key = vm.operand_stack.pop();
                exp = vm.operand_stack.pop();
                obj[key] = exp;
                if (exp instanceof Closure) {
                    obj.__METHODS__ = obj.__METHODS__ || [];
                    obj.__METHODS__.push(key);
                }
            }
            vm.operand_stack.push(obj);
            vm.pc++;
        }
    },
    LDCA: {
        param: 1,
        execute: function(vm, n) {
            var arr = [];
            for (var i = 0; i < n; i++) {
                arr.push(vm.operand_stack.pop());
            }
            arr.reverse();
            vm.operand_stack.push(arr);
            vm.pc++;
        }
    },
    READPS: {
        param: 0,
        execute: function(vm) {
            var id = vm.operand_stack.pop();
            var rec = vm.operand_stack.pop();
            var value;
            if (rec instanceof Closure) {
                if (rec.is_native) {
                    value = FFI.get_function(vm.ffi_register, rec.address);
                    value = value[id];
                } else {
                    value = rec.mock[id];
                }
            } else {
                value = rec[id];
            }
            vm.operand_stack.push(value);
            vm.pc++;
        }
    },
    STOREPS: {
        param: 0,
        execute: function(vm) {
            var value = vm.operand_stack.pop();
            var key = vm.operand_stack.pop();
            var rec = vm.operand_stack.pop();
            if (rec instanceof Closure) {
                if (rec.is_native) {
                    FFI.get_function(rec.address, vm.ffi_register)[key] = value;
                } else {
                    rec.mock[key] = value;
                }
            } else {
                rec[key] = value;
                if (value instanceof Closure) {
                    rec.__METHODS__ = rec.__METHODS__ || [];
                    rec.__METHODS__.push(key);
                }
            }
            vm.pc++;
        }
    },
    STORE: {
        param: 1,
        execute: function(vm, id) {
            var exp = vm.operand_stack.pop();
            if (id !== "undefined") {
                vm.environment.update(id, exp);
            }
            vm.pc++;
        }
    },
    DONE: {
        param: 0
    }
};
});
