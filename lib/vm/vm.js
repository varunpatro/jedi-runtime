/* Indicate that this file is run in worker thread */
importScripts('../../node_modules/requirejs/require.js');

/* Import necessary javascript files that does not modify DOM */

require({ baseUrl: "../../" },
        ["lib/vm/runtime", 'lib/messages', 'lib/messageLibrary'],
function(Runtime, Thread, Msg) {
    "use strict";
    var vm;
    Thread.listen()
    .when(Msg.VM.Request.CREATE_INSTANCE, function(url, imports) {
        if (url !== undefined && url !== "") {
            importScripts(url);
        }
        vm = new Runtime(imports);
        Thread.broadcast(Msg.VM.Response.INSTANCE_CREATED,
                                vm.to_transferable_object());
    })
    .when(Msg.VM.Request.SET_INSTANCE, function(vm_instance) {
        vm.clone_transferable_object(vm_instance);
        Thread.broadcast(Msg.VM.Response.INSTANCE_SET);
    })
    .when(Msg.VM.Request.EXECUTE_INSTRUCTION,
          function(instructionArray, start, suspend) {
        if (vm === undefined) {
           Thread.broadcast(Msg.VM.Response.INSTANCE_UNDEFINED);
        } else {
            var result = vm.execute_instruction(instructionArray, start,
                                                suspend);
            Thread.broadcast(result.status, result.value);
        }
    })
    .when(Msg.VM.Request.GET_RUNTIME_INFORMATION, function() {
        if (vm === undefined) {
            Thread.broadcast(Msg.VM.Response.INSTANCE_UNDEFINED);
        } else {
            Thread.broadcast(Msg.VM.Response.RUNTIME_INFORMATION,
                             vm.get_runtime_information());
        }
    })
    .when(Msg.VM.Request.RESUME_EXECUTION, function(stop) {
        if (vm === undefined) {
            Thread.broadcast(Msg.VM.Response.INSTANCE_UNDEFINED);
        } else {
            var result = vm.execute_instruction(vm.instruction_array, vm.pc, stop);
            Thread.broadcast(result.status, result.value);
        }
    })
    .when(Msg.Main.Response.FFI_CALL_DONE, function(ffi_result) {
        var suspend_due_to_breakpoint = vm.resume_from_ffi(ffi_result);
        var result = suspend_due_to_breakpoint ||
                        vm.execute_instruction(vm.instruction_array, vm.pc);
        Thread.broadcast(result.status, result.value);
    });
    Thread.broadcast(Msg.VM.Response.MODULE_LOADED);
});

