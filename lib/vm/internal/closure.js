/*globals define */
/** @module closure
 *  Describe the Closure class.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define([], function() {
"use strict";

function Closure(address, formals, environment, source) {
    this.address = address;
    this.formals = formals;
    this.environment = environment;
    this.is_native = false;
    this.is_external = false;
    this.source = source;
}

function build_arguments_string(args) {
    var str = "";
    var i;
    for (i = 1; i < args.length; i++) {
        if (i === args.length - 1) {
            str = str + args[i];
        } else {
            str = str + args[i] + ", ";
        }
    }
    return str;
}

Closure.prototype.toString = function() {
    if (!this.is_native) {
        return "function(" + build_arguments_string(this.formals) +
            ") { [body] }";
    } else {
        return this.code_string;
    }
};

return Closure;
});
