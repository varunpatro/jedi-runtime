/*globals define */
/** @module environment
 *  Describe the environment class.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(['lib/vm/exception', 'lib/vm/internal/closure'],
function(Exception, Closure) {
"use strict";

/** @class Environment
 *  Register for variables.
 */

/**
 *  Constructor.
 *  @param {Environment} parent The enclosing environment.
 *  Global environment has "null" as its parent.
 */
function Environment(parent) {
    this.parent = parent;
    this.__env  = {};
    this.is_closure_formals = false;
}

/**
 *  Get a value of a variable from this or enclosing environment(s).
 *  @param {String} name The name of the variable
 *  @throws UnboundVariable if the variable does not exists.
 *  @returns {Any} The value of the variable
 */
Environment.prototype.get = function(name) {
    var env = this;
    while (env !== null) {
        if (env.__env.hasOwnProperty(name)) {
            return env.__env[name];
        }
        env = env.parent;
    }
    throw new Exception.UnboundVariable(name);
};

/**
 * Add  a new variable binding to this environment.
 * @param {String} name The name of the variable
 * @param {Any} name Value of the variable.
 */
Environment.prototype.add = function(name, value) {
    this.__env[name] = value;
};

/**
 *  Update a variable with a new value from this or enclosing environment(s).
 *  @throws UnboundVariable if the variable does not exist
 */
Environment.prototype.update = function(name, newValue) {
    var env = this;
    while (env !== null) {
        if (env.__env.hasOwnProperty(name)) {
            env.__env[name] = newValue;
            return;
        }
        env = env.parent;
    }
    throw new Exception.UnboundVariable(name);
};

/**
 *  Chain update a property of an object from the environment.
 *  @throws UnboundVariable if the variable does not exist
 */
Environment.prototype.chain_update = function(names, newValue) {
    var obj = this.get(names[0]);
    var lastLookup = names[names.length - 1];
    var i;
    for (i = 1; i < names.length - 1; ++i) {
        obj = obj[names[i]];
        if (obj === undefined) {
            throw new Exception.UnboundVariable(names[i]);
        }
    }
    if (obj[lastLookup] === undefined) {
        throw new Exception.UnboundVariable(lastLookup);
    }
    obj[lastLookup] = newValue;
};

/**
 * Check if the argument supplied is a Closure
 * @param {any} arg The variable to be checked
 * @return {boolean} True if the argument supplied is a Closure,
 *                   false otherwise
 */
Environment.is_closure = function(arg) {
    return arg instanceof Closure;
};

/**
 * Check if the argument supplied is an object
 * @param {any} arg The variable to be checked
 * @return {boolean} True if the argument supplied is an object,
 *                   false otherwise
 */
Environment.is_object = function(arg) {
    return typeof(arg) === typeof({});
};

/**
 *  Return a transferrable object from this instance.
 */
Environment.prototype.to_transferable_object = function() {
    return this.__env;
};

/**
 *  Clone a transferrable object version of this instance.
 */
Environment.prototype.clone_transferable_object = function(env) {
    this.__env = env;
};

return Environment;
});
