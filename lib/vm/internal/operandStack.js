/*globals define */
/** @module operandStack
 *  Defines the OperandStack class.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(['lib/vm/exception'], function(Exception) {
"use strict";

/** @class OperandStack
 *  Registers for operands.
 */
function OperandStack() {
    this.stack = [];
}

/**
 *  Get the topmost value of the operand stack.
 *  @throws EmptyOperandStack
 *  @returns {any} Topmost value of the operand stack.
 */
OperandStack.prototype.top = function() {
    return this.at(0);
};

/**
 *  Get a n'th last inserted value from the operand stack. 0 is the latest
 *  @throws OperandStackIndexOutOfBounds If the index is out of bounds.
 *  @throws EmptyOperandStack If the stack is empty.
 *  @returns {any} N'th last inserted item in the operand stack.
 */
OperandStack.prototype.at = function(index) {
    if (this.stack.length <= 0) {
        throw new Exception.EmptyOperandStack();
    }
    var actual_index = this.stack.length - 1 - index;
    if (index < 0 || actual_index < 0) {
        throw new Exception.OperandStackIndexOutOfBounds();
    }
    return this.stack[actual_index];
};

/**
 *  Push a value to the operand stack.
 *  @param {any} value The value to be pushed.
 */
OperandStack.prototype.push = function (value) {
    this.stack.push(value);
};

/**
 *  Pop and return a value from the operand stack.
 *  @throws EmptyOperandStack
 *  @returns {any} The topmost value from the operand stack.
 */
OperandStack.prototype.pop = function () {
    if (this.stack.length === 0) {
        throw new Exception.EmptyOperandStack();
    }
    return this.stack.pop();
};

/**
 *  Return a transferrable object from this instance.
 */
OperandStack.prototype.to_transferable_object = function() {
    return this.stack;
};

/**
 *  Clone a transferrable object version of this instance.
 */
OperandStack.prototype.clone_transferable_object = function(stack) {
    this.stack = stack;
};

return OperandStack;
});
