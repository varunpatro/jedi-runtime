/*globals define */
/** @module stackFrame
 *  Describe the StackFrame class.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define([], function() {
"use strict";

function StackFrame(pc, environment, operand_stack, is_object_construction) {
    this.pc = pc;
    this.environment = environment;
    this.operand_stack = operand_stack;
    this.is_object_construction = is_object_construction || false;
}

/**
 *  Return a transferrable object from this instance.
 */
StackFrame.prototype.to_transferable_object = function() {
    return {
        pc: this.pc,
        environment: this.environment.to_transferable_object(),
        operand_stack: this.operand_stack.to_transferable_object(),
        is_object_construction: this.is_object_construction
    };
};

return StackFrame;
});
