/*globals define */
/** @module instruction
*   Defines the Instruction class.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
*/
define([], function() {
"use strict";

/** Instruction constructor.
 *  @param {String} name The name of the instruction as defined in
 *                     instructionData.js.
 *  @param {any} value The argument for the instruction if needed.
 *  @param {String} source Optionally, the source corresponds to the instruction.
 */
function Instruction(name, value, source) {
    this.name = name;
    this.value = value;
    this.source = source;
}

return Instruction;
});
