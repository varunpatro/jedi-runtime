/*globals define */
/** @module exception
 *  Defines the exception classes for the virtual machine.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define([], function() {
"use strict";

return {
    InstructionNotFound: function(instructionName) {
        this.message = instructionName + " does not exist in the instruction" +
            " library";
    },

    OperandStackIndexOutOfBounds: function() {
        this.message = "Operand stack index out of bounds";
    },

    EmptyOperandStack: function() {
        this.message = "Popping from empty operand stack.";
    },

    WrongNumberOfArguments: function(given, needed) {
        this.message = "Wrong number of arguments supplied.\n";
        this.message += (needed - 1) + " needed, " + (given - 1) + " given.";
    },

    TypeError: function(expectedType, actualType, which) {
        this.message =  "This expression does not conform to the official " +
            "Jediscript specification\n";
        if (which) {
          this.message += "Expected " + which + " operand of type " +
            expectedType + " but get " + actualType;
        } else {
          this.message += "Expected expression of type " + expectedType +
              " but get " + actualType;
        }
    },

    MaximumCallStackSizeExceeded: function() {
      this.message = "Maximum call stack size exceeded";
    },

    UnboundVariable: function(variable) {
        this.message = "Unbound variable: " + variable;
    }
};
});
