/*globals define, window */
/** @module ffi
 *  Foreign (javascript) function interface.
 *  Store, manage, and call native javascript primitives/functions.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(function(require) {
"use strict";

var Closure = require('lib/vm/internal/closure');
var OperandStack = require('lib/vm/internal/operandStack');
var Instruction = require('lib/vm/instruction');

/**
 *  Get a javascript function from the register.
 *  @param {function} address The address of the function.
 *  @returns {Closure} The body of the function.
 */
function get_function(register, address) {
    return register[address];
}

/**
 *  Register a javascript function.
 *  @param {function} body The body of the function.
 *  @returns {Closure} The native closure of the function.
 */
function register_internal_function(register, body) {
    register.push(body);
    var closure = new Closure(register.length - 1);
    closure.code_string = body + "";
    closure.is_native = true;
    closure.is_external = false;
    return closure;
}

/**
 *  Register an external javascript function into a register.
 *  @param {function} body The body of the function.
 *  @returns {Closure} The native closure of the function.
 */
function register_external_function(register, body) {
    var closure = register_internal_function(register, body);
    closure.is_external = true;
    return closure;
}

function constructor_to_vm_call(vm, closure) {
    return function() {
        var nargs = arguments.length;
        var old_pc = vm.pc;
        var old_operand_stack = vm.operand_stack;
        vm.operand_stack = new OperandStack();
        vm.operand_stack.push(closure);
        var i;
        for (i = 0; i < nargs; i++) {
            vm.operand_stack.push(arguments[i]);
        }
        vm.instruction_array.push(new Instruction("CALL", nargs));
        var result = vm.execute_instruction(vm.instruction_array,
                                            vm.instruction_array.length - 1);
        vm.instruction_array.pop();
        vm.operand_stack = old_operand_stack;
        vm.pc = old_pc;
        return result.value;
    };
}

/* When a closure is passed to native function call, wrap it to a function
 * that emulates a VM function call. It will move pc to closure address
 * and extend the environment as usual
 */
function wrap_to_vm_call(vm, closure) {
    var dummy_caller = function() {
        var nargs = arguments.length + 1;
        var old_pc = vm.pc;
        var old_operand_stack = vm.operand_stack;
        vm.operand_stack = new OperandStack();
        vm.operand_stack.push(closure);
        vm.operand_stack.push(this);
        var i;
        for (i = 0; i < nargs - 1; i++) {
            vm.operand_stack.push(arguments[i]);
        }
        vm.instruction_array.push(new Instruction("CALL", nargs));
        var result = vm.execute_instruction(vm.instruction_array,
                      vm.instruction_array.length - 1);
        vm.instruction_array.pop();
        vm.operand_stack = old_operand_stack;
        vm.pc = old_pc;
        return result.value;
    };
    dummy_caller.call = constructor_to_vm_call(vm, closure);
    dummy_caller.__as_closure__ = closure;
    dummy_caller.toString = function() {
        return vm.stringify_value(closure);
    };
    return dummy_caller;
}

function vm_object_to_javascript(vm, obj) {
    if (typeof obj !== "object" ||
        obj === null || obj === undefined ||
        obj.__NATIVE_OBJECT__ !== undefined) {
        return obj;
    }
    if (obj.hasOwnProperty("__METHODS__")) {
        obj.__METHODS__.forEach(function (key) {
            if (obj.hasOwnProperty(key)) {
                obj[key] = vm_value_to_javascript(vm, obj[key]);
            }
        });
    }
    obj.__proto__ = vm_object_to_javascript(vm, obj.__proto__); // jshint ignore:line
    return obj;
}

/**
 * Ensure a JediScript value is inspectable/callable in native function.
 * For instance, Closure needs to be wrapped to VM call.
 * @param {any} arg The argument to be converted.
 * @param {object} vm The virtual machine callee.
 * @return {any} The converted argument.
*/
function vm_value_to_javascript(vm, arg) {
    if (arg instanceof Closure && !arg.is_native) {
        return arg.mock;
    } else if (arg instanceof Closure && arg.is_native) {
        return vm.ffi_register[arg.address];
    } else if (arg instanceof Array) {
        return arg;
    } else {
        switch (typeof arg) {
            // Simple values can be passed directly.
        case 'number':
        case 'string':
        case 'boolean':
        case 'function':
            return arg;
        case 'object':
            return vm_object_to_javascript(vm, arg);
        default:
            return arg;
        }
    }
}

return {
    wrap_to_vm_call: wrap_to_vm_call,
    get_function: get_function,
    vm_value_to_javascript : vm_value_to_javascript,
    /**
     *  Import external (DOM modifying) JavaScript values to the
     *  global environment of the VM.
     *
     *  Caveat : VM instance here is the transferrable object version,
     *  meaning that it is a raw object and thus doesn't have access to
     *  any of its methods.
     *
     *  @param {object} vm The VM instance to be initialized.
     *  @param {array}  externals List of external identifiers.
     *  @return {array} Array of native function closures.
     */
    import_dom_modifying_function: function(vm, externals) {
        var ffi_register = [];
        externals.forEach(function(name) {
            var value = window[name];
            //! If it is native function, register it to be called later
            if (typeof value === "function") {
                var closure = register_external_function(ffi_register, value);
                vm.environment[name] = closure;
            } else {
                vm.environment[name] = value;
            }
        });
        return ffi_register;
    },

    /**
     *  Import internal (non DOM modifying) JavaScript values to the
     *  global environment of the VM.
     *
     *  @param {object} vm The VM instance to be initialized.
     *  @param {array}  internals List of internal value identifiers.
     *  @return {array} Array of native function closures.
     */
    import_non_dom_modifying_function: function(vm, internals, context) {
        var ffi_register = [];
        internals.forEach(function(name) {
            var value;
            if (vm.is_inside_web_worker) {
                value = self[name];
            } else {
                value = context[name];
            }
            //! If it is native function, register it to be called later
            if (typeof value === "function") {
                var closure = register_internal_function(ffi_register, value);
                vm.environment.add(name, closure);
            } else if (typeof value === "object") {
                value.__NATIVE_OBJECT__ = true;
                vm.environment.add(name, value);
            } else {
                vm.environment.add(name, value);
            }
        });
        return ffi_register;
    },

    /**
     *  Call a native javascript function.
     *  @param {number} address The address of the function in the register.
     *  @param {array} args The arguments of the function call.
     *  @param {array} register The native function register.
     *  @param {boolean} is_external_call Whether the call is executed
     *  externally in the main thread.
     *  @return {any} The result of the FFI call.
     */
    call: function(address, args, register, is_external_call) {
        is_external_call = is_external_call || false;
        var fun = get_function(register, address);
        return this.call_wo_register(fun, args, register, is_external_call);
    },

    /**
     *  Call a native javascript object's without register.
     *  This is the case when calling native javascript object methods.
     *  Object method is not registered to FFI register.
     *  As a consequence, they stay raw instead of in the form of a closure.
     *  @param {function}  fun The method to be called.
     *  @param {array} args The arguments of the function call.
     *  @return {any} The result of the FFI call.
     */
    call_wo_register: function(fun, args, register, is_external_call) {
        var thisCtx = args[0];
        Array.prototype.shift.apply(args);
        var returnValue = fun.apply(thisCtx, args.map(function (arg) {
            if (arg instanceof Closure) {
                return get_function(register, arg.address);
            } else {
                return arg;
            }
        }));
        if (typeof returnValue === "function") {
            if (!is_external_call) {
                return register_internal_function(register, returnValue);
            } else {
                return register_external_function(register, returnValue);
            }
        } else {
            return returnValue;
        }
    },

    /**
     *  Construct an object from foreign constructor.
     *  Similar to FFI Call, but this call the function according
     *  to JavaScript "new" statement semantics.
     *  @param {function}  fun The method to be called.
     *  @param {array} args The arguments of the function call.
     *  @param {array} register The FFI register.
     *  @return {any} The result of the FFI object construction.
     */
    construct_foreign_object: function(closure, args, register) {
        var obj = {};
        var Constructor = get_function(register, closure.address);
        obj.constructor = Constructor;
        obj.__proto__ = Constructor.prototype; // jshint ignore:line
        Array.prototype.shift.apply(args);
        var returnValue = Constructor.apply(obj, args.map(function (arg) {
            if (arg instanceof Closure) {
                return get_function(register, arg.address);
            } else {
                return arg;
            }
        }));
        if (returnValue === undefined) {
            return obj;
        } else if (typeof returnValue == "function") {
            if (!is_external_call) {
                return register_internal_function(register, returnValue);
            } else {
                return register_external_function(register, returnValue);
            }
        } else {
            return returnValue;
        }
    }
};
});
