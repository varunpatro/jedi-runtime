/*globals define */
/** @module runtime
 *  The main loop of the virtual machine.
 *  Also stores debugging information needed by the debugger.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
 */
define(function(require) {
"use strict";

var Exception = require('lib/vm/exception');
var OperandStack = require('lib/vm/internal/operandStack');
var Environment = require('lib/vm/internal/environment');
var Closure = require('lib/vm/internal/closure');
var FFI = require('lib/vm/ffi');
var instructionData = require('lib/vm/instructionData');
var Msg = require('lib/messageLibrary');

/**
 * @class Runtime
 * The main loop of the virtual machine.
 * @param {array} imports List of native values identifiers to be included.
 * @param {object} context Context of the VM, defaults to Window if unspecified.
 */
function Runtime(imports, context) {
    context = context || self;
    this.instruction_array = [];
    this.runtime_stack = [];
    this.operand_stack = new OperandStack();
    this.environment  = new Environment(null);
    this.pc = 0;
    this.is_inside_web_worker = typeof WorkerGlobalScope !== 'undefined' &&
        self instanceof WorkerGlobalScope;
    if (imports !== undefined) {
        this.ffi_register =
          FFI.import_non_dom_modifying_function(this, imports, context);
    } else {
        this.ffi_register = [];
    }
}

/**
 *  Loads and execute the instructions in the instruction_array
 *  @param {array} instruction_array The instruction array to be executed.
 *  @param {number} start The starting point of the code
 *  @param {object} stops An object consisting the instruction numbers
 *                        to suspend execution at as the property and
 *                        the boolean true as the value
 *  @return {object} Object containing the status of the execution, and the top
 *  of the operand stack if the execution is finished.
 *
 */
Runtime.prototype.execute_instruction = function(instruction_array, start, stops) {
    if (stops === undefined) {
        stops = {};
    }
    this.pc = start || 0;
    this.instruction_array = instruction_array;
    var instruction;
    while (this.pc < instruction_array.length &&
             instruction_array[this.pc].name !== "DONE") {
        instruction = instruction_array[this.pc];
        var ins = instructionData[instruction.name];
        if (ins === undefined) {
            throw new Exception.InstructionNotFound(instruction.name);
        }
        var result = ins.execute(this, instruction.value, instruction.source);
        // FFI call to DOM modifying function detected.
        if (typeof result === 'object') {
            return result;
        }
        // Check if next instruction is a breakpoint.
        if (stops[this.pc]) {
            return {
                status: Msg.VM.Response.EXECUTION_SUSPENDED
            };
        }
    }
    return {
        status: Msg.VM.Response.EXECUTION_FINISHED,
        value: this.operand_stack.top()
    };
};

/**
 *  Give the VM more instructions to execute.
 *  The instructions should NOT be normalized prior, the VM will do it.
 *  @return {object} The execution result.
 */
Runtime.prototype.execute_more_instruction = function(instructions) {
    var base = this.instruction_array.length;
    var normalized_ldf = instructions.map(function(instruction) {
        if (instruction.name === "LDF") {
            instruction.value[0] = instruction.value[0] + base;
        }
        return instruction;
    });
    this.pc = base;
    this.instruction_array = this.instruction_array.concat(normalized_ldf);
    return this.execute_instruction(this.instruction_array, base);
};

/**
 *  Gets the current runtime information for debugging purposes.
 *  @return {object} Objects containing current runtime environment.
 */
Runtime.prototype.get_runtime_information = function() {
    return {
        pc: this.pc,
        operand_stack: this.operand_stack.to_transferable_object(),
        runtime_stack: this.runtime_stack.map(function(s) {
            return s.to_transferable_object();
        }),
        environment: this.environment.to_transferable_object()
    };
};

/**
 * Return a message object Requesting for FFI Call
 * @param {number} address The address of the function to call.
 * @param {args} args The list of arguments of the function call.
 * @return {object} A message representing an FFI call request.
 */
Runtime.prototype.suspend_for_ffi = function(address, args) {
    return {
        status: Msg.Main.Request.FFI_CALL,
        value: {
            address: address,
            args: args
        }
    };
};

/**
 * Resume from an external FFI function call
 * @param {result} ffi_result The result of the FFI Call.
 */
Runtime.prototype.resume_from_ffi = function(ffi_result) {
    this.operand_stack.pop();
    this.operand_stack.push(ffi_result);
    this.pc++;
};

/**
 * Return a transferrable object from this instance.
 */
Runtime.prototype.to_transferable_object = function() {
    return {
        operand_stack: this.operand_stack.to_transferable_object(),
        environment: this.environment.to_transferable_object(),
        pc: this.pc,
        is_inside_web_worker: this.is_inside_web_worker,
        instruction_array: this.instruction_array
    };
};

/**
 * Clone a transferrable object version of this instance.
 */
Runtime.prototype.clone_transferable_object = function(vm_instance) {
    this.operand_stack.clone_transferable_object(vm_instance.operand_stack);
    this.environment.clone_transferable_object(vm_instance.environment);
    this.pc = vm_instance.pc;
    this.instruction_array = vm_instance.instruction_array;
};

/**
 * Get a clone of this runtime instance
 */
Runtime.prototype.get_clone = function() {
    var runtime = new Runtime([]);
    runtime.clone_transferable_object(this.to_transferable_object());
    return runtime;
};

function rename_code_to_program(str) {
    return str.replace(/\[native code\]/i, "[body]");
}

/**
 * Clone a transferrable object version of this instance.
 */
Runtime.prototype.stringify_value = function(value) {
    if (value === undefined) {
        return "undefined";
    } else if (value instanceof Closure) {
        return value.source || (rename_code_to_program(value.toString()));
    } else {
        return value;
    }
};

/**
* Reset the VM operand stack
*/
Runtime.prototype.reset_operand_stack = function() {
    this.operand_stack = new OperandStack();
};

/**
 * Convert a VM value to Javascript value.
 */
Runtime.prototype.vm_value_to_javascript = function(value) {
    return FFI.vm_value_to_javascript(this, value);
};

Runtime.prototype.dump_instructions = function () {
    var str = "\n";
    var pc = 0;
    this.instruction_array.forEach(function (instr) {
        str += "" + pc + " " + instr.name + " " + (instr.value || "") + "\n";
        pc++;
    });
    return str;
};

return Runtime;
});
