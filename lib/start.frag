(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        //Allow using this built library as an AMD module
        //in another project. That other project will only
        //see this AMD call, not the internal modules in
        //the closure below.
        define([], factory);
    } else if (typeof process !== 'undefined' && process.argv[0].indexOf("node") !== -1) {
        global.JediRuntime = factory();
    } else {
        //Browser globals case. Just assign the
        //result to a property on the global.
        root.JediRuntime = factory();
    }
}(this, function () {
