/** @module messageLibrary
*   Defines the message passing constants between runtime and main window.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
*/
define([], function() {
"use strict";

return {
    VM: {
        Request: {
            CREATE_INSTANCE: "create instance",
            EXECUTE_INSTRUCTION: "execute instruction",
            SET_INSTANCE: "set instance",
            RESUME_EXECUTION: "resume execution",
            GET_RUNTIME_INFORMATION: "get runtime information"
        },
        Response: {
            MODULE_LOADED: "module loaded",
            INSTANCE_CREATED: "instance created",
            INSTANCE_UNDEFINED: "instance undefined",
            INSTANCE_SET: "instance set",
            EXECUTION_FINISHED: "execution finished",
            EXECUTION_FAILED: "execution failed",
            EXECUTION_SUSPENDED: "execution suspended",
            RUNTIME_INFORMATION: "runtime information"
        }
    },

    Main: {
        Request: {
            FFI_CALL: "ffi call"
        },
        Response: {
            FFI_CALL_DONE: "ffi call done"
        }
    }
};
});
