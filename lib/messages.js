/* Messages */
/** @module messages
*   Defines the message passing between runtime and main window.
 *  @author Evan Sebastian <evanlhoini@gmail.com>
 *  @author Diga Widyaprana <digawp@gmail.com>
*/
define([], function() {
"use strict";

function MessageNotFound() {
    this.message = "Message is not defined in the library." +
                   "Check messageLibrary.js again";
}

function thread_has_no_subscription(thread) {
    return typeof thread.onmessage !== 'function';
}

function Subscription(thread) {
    this.thread = thread;
    if (thread_has_no_subscription(this.thread)) {
        this.thread.__events = [];
        var t = this.thread;
        t.onmessage = function (event) {
            t.__events.forEach(function (e) {
                if (e.type === event.data.type) {
                    e.callback.apply(t, event.data.params);
                }
            });
        };
    }
}

Subscription.prototype.when = function(message, callback) {
    if (message === undefined) {
        throw new MessageNotFound();
    } else {
        this.thread.__events.push({ type: message, callback: callback });
    }
    return this;
};

return {
    /**
     * Subscribe to a thread and define handlers for each message.
     * @example
     *
     * var foo = new Worker('foo.js');
     * Msg.subscribe(foo)
     *    .when(WAKE_UP, function() {
     *          console.log("Good morning");
     *    });
     *
     * When thread foo calls Msg.broadcast("WAKE_UP");, "Good morning" will be
     * logged to the console.
     *
     * @param {object} thread Thread / broadcaster to subscribe
     * callback function as a response.
     * @return {object} A promise which wrap the thread.
     */
    subscribe: function(thread) {
        return new Subscription(thread);
    },

    /**
     * Send message to a thread, this is similar to adding that thread as
     * a subscriber, broadcasting the message, and unsubscribing it.
     * @example Msg.send_message(calculator, ADD, 2, 3);
     */
    send_message: function () {
        if (arguments.length < 2) {
            console.log("Invalid send_message");
            return;
        } else {
            var recipient = arguments[0];
            var message = arguments[1];
            var param;
            if (arguments.length > 2 ) {
                param = Array.prototype.slice.call(arguments, 2);
            } else {
                param = [];
            }
            if (message !== undefined) {
                recipient.postMessage({ type: message, params: param });
            } else {
                throw new MessageNotFound();
            }
        }
    },

    /**
     * Listen to events and define the response.
     */
    listen: function() {
        return this.subscribe(self);
    },

    /**
    * Broadcast message to subscriber
    * @example Msg.broadcast(EXECUTION_FINISHED, result);
    */
    broadcast: function () {
        if (arguments.length < 1) {
            console.log("Invalid broadcast");
            return;
        } else {
            var message = arguments[0];
            var param;
            if (arguments.length > 1 ) {
                param = Array.prototype.slice.call(arguments, 1);
            } else {
                param = [];
            }
            if (message !== undefined) {
                postMessage({ type: message, params: param });
            } else {
                throw new MessageNotFound();
            }
        }
    }
};
});
