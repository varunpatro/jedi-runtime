define([
	'lib/parser/nodes',
	'lib/vm/instruction'
], function (Nodes, Instruction){


function make_goto_instruction(offset) {
	return new Instruction('GOTOR', offset);
}

function make_goto_on_false_instruction(offset) {
	return new Instruction('JOFR', offset);
}

function make_return_instruction() {
	return new Instruction('RTN');
}

function make_declare_symbol_instruction(name) {
	return new Instruction('DECLARE', name);
}

function make_store_symbol_instruction(name) {
	return new Instruction('STORE', name);
}

function make_load_symbol_instruction(name) {
	return new Instruction('LDS', name);
}

function make_load_function_instruction(configuration, source) {
	return new Instruction('LDF', configuration, source);
}

function make_call_instruction(argument_count) {
	return new Instruction('CALL', argument_count);
}

function make_tail_call_instruction(argument_count) {
	return new Instruction('TAILCALL', argument_count);
}

function make_done_instruction() {
	return new Instruction('DONE');
}

function make_read_object_property_instruction() {
	return new Instruction('READPS');
}

function make_store_object_property_instruction() {
	return new Instruction('STOREPS');
}

function make_load_literal_object_instruction(count) {
	return new Instruction('LDCO', count);
}

function make_load_literal_array_instruction(count) {
	return new Instruction('LDCA', count);
}

function make_construction_call_instruction(count) {
	return new Instruction('CONSTRUCT', count);
}

function make_load_constant_value_instruction(value) {
	if (Nodes.is_string(value)) {
		return new Instruction('LDCS', value);
	} else if (Nodes.is_number(value)) {
		return new Instruction('LDCN', value);
	} else if (Nodes.is_boolean(value)) {
		return new Instruction('LDCB', value);
	} else if (Nodes.is_undefined_value(value)) {
		return new Instruction('LDU');
	} else {
		throw new Util.CompileError('Unexpected primitive value', value);
	}
}

return {
	make_goto_instruction: make_goto_instruction,
	make_goto_on_false_instruction: make_goto_on_false_instruction,
	make_return_instruction: make_return_instruction,
	make_declare_symbol_instruction: make_declare_symbol_instruction,
	make_store_symbol_instruction: make_store_symbol_instruction,
	make_load_symbol_instruction: make_load_symbol_instruction,
	make_load_function_instruction: make_load_function_instruction,
	make_call_instruction: make_call_instruction,
	make_tail_call_instruction: make_tail_call_instruction,
	make_done_instruction: make_done_instruction,
	make_read_object_property_instruction: make_read_object_property_instruction,
	make_store_object_property_instruction: make_store_object_property_instruction,
	make_load_literal_object_instruction: make_load_literal_object_instruction,
	make_load_literal_array_instruction: make_load_literal_array_instruction,
	make_construction_call_instruction: make_construction_call_instruction,
	make_load_constant_value_instruction: make_load_constant_value_instruction
};

});
