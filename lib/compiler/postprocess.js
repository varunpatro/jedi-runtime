define([
	'lib/compiler/util',
	'lib/compiler/nodecompiler',
	'lib/compiler/instructionfactory',
	'lib/compiler/pseudoinstruction'],
function (Util, NodeCompiler, InstructionFactory, PseudoInstruction) {

function post_process(compile_result) {
	// Record all address label, which translates to no instruction at all
	var label_address_table = {};
	var label_prefix_count = [];
	var temporary_variables = Util.make_hash_set();
	var i;
	var end;

	for (i = 0, end = compile_result.instructions.length; i < end; ++i) {
		if (PseudoInstruction.is_store_temporary_variable_label(compile_result.instructions[i]) ||
					PseudoInstruction.is_load_temporary_variable_label(compile_result.instructions[i])) {
			Util.add_to_hash_set(temporary_variables, PseudoInstruction.label_name(compile_result.instructions[i]));
		}
	}

	compile_result.debug =
		temporary_variables.values
			.map(
				function(){
					return Util.make_debug_info(0);
				})
			.concat(compile_result.debug);
	compile_result.instructions =
		temporary_variables.values
			.map(InstructionFactory.make_declare_symbol_instruction)
			.concat(compile_result.instructions);

	i = 0;
	end = compile_result.instructions.length;
	var label_count = 0;
	while (i < end) {
		if (PseudoInstruction.is_address_label(compile_result.instructions[i])) {
			var labels = [];
			// Seek ahead as much as possible because
			// there can be more than one address label
			// referring to the same instruction
			while (i < end && PseudoInstruction.is_address_label(compile_result.instructions[i])) {
				labels.push(PseudoInstruction.label_name(compile_result.instructions[i]));
				label_prefix_count.push(++label_count);
				++i;
			}
			for (var j = 0; j < labels.length; ++j) {
				label_address_table[labels[j]] = i;
			}
		}
		label_prefix_count.push(label_count);
		++i;
	}

	var instructions = [];
	var debug = [];

	function replace_goto_label(instructions, label, current_address) {
		var target = label_address_table[PseudoInstruction.label_name(label)];
		var skipped = label_prefix_count[target] - label_prefix_count[current_address];
		instructions.push(
			InstructionFactory.make_goto_instruction(
				target - current_address - skipped));
	}

	function replace_false_goto_label(instructions, label, current_address) {
		var target = label_address_table[PseudoInstruction.label_name(label)];
		var skipped = label_prefix_count[target] - label_prefix_count[current_address];
		instructions.push(
			InstructionFactory.make_goto_on_false_instruction(
				target - current_address - skipped));
	}

	function replace_return_label(instructions, current_address) {
		instructions.push(InstructionFactory.make_return_instruction());
	}

	function replace_function_load_label(instructions, label) {
		var name = PseudoInstruction.label_name(label);
		var target = label_address_table[name];
		var skipped = label_prefix_count[target];
		var function_description = PseudoInstruction.function_load_label_description(label);
		var source = function_description.source;

		instructions.push(
			InstructionFactory.make_load_function_instruction(
				[target - skipped]
					.concat(function_description.parameters), source));
	}

	function replace_call_label(instructions, label, current_address) {
		var jump_trace = current_address + 1;
		while (jump_trace < end) {
			var instruction = compile_result.instructions[jump_trace];

			if (PseudoInstruction.is_goto_label(instruction) ||
					PseudoInstruction.is_address_label(instruction)) {
				jump_trace = label_address_table[PseudoInstruction.label_name(instruction)];
			} else {
				break;
			}
		}
		if (PseudoInstruction.is_return_label(compile_result.instructions[jump_trace])) {
			instructions.push(
				InstructionFactory.make_tail_call_instruction(
					PseudoInstruction.call_label_argument_length(label)));
		} else {
			instructions.push(
				InstructionFactory.make_call_instruction(
					PseudoInstruction.call_label_argument_length(label)));
		}
	}

	function replace_load_temporary_variable_label(instructions, label) {
		instructions.push(
			InstructionFactory.make_load_symbol_instruction(
				PseudoInstruction.label_name(instruction)));
	}

	function replace_store_temporary_variable_label(instructions, label) {
		instructions.push(
			InstructionFactory.make_store_symbol_instruction(
				PseudoInstruction.label_name(instruction)));
	}

	for(i = 0, end = compile_result.instructions.length; i < end; ++i) {
		var instruction = compile_result.instructions[i];
		if (PseudoInstruction.is_address_label(instruction)) {
			continue;	// Skip all address labels
		} else if (PseudoInstruction.is_goto_label(instruction)) {
			replace_goto_label(instructions, instruction, i);
		} else if (PseudoInstruction.is_false_goto_label(instruction)) {
			replace_false_goto_label(instructions, instruction, i);
		} else if (PseudoInstruction.is_call_label(instruction)) {
			replace_call_label(instructions, instruction, i);
		} else if (PseudoInstruction.is_return_label(instruction)) {
			replace_return_label(instructions, i);
		} else if (PseudoInstruction.is_function_load_label(instruction)) {
			replace_function_load_label(instructions, instruction);
		} else if (PseudoInstruction.is_load_temporary_variable_label(instruction)) {
			replace_load_temporary_variable_label(instructions, instruction);
		} else if (PseudoInstruction.is_store_temporary_variable_label(instruction)) {
			replace_store_temporary_variable_label(instructions, instruction);
		} else {
			instructions.push(instruction);
		}

		debug.push(compile_result.debug[i]);
	}

	return {
		instructions: instructions,
		debug: debug
	};
}

return {
	post_process: post_process
};

});
