define(function() {

return {
    ClosureFormalRedeclarationException: function(name, line) {
        this.line = line;
        this.message = "\nOn line : " + line + "\n";
        this.message = "Trying to redefine function formal parameter: ";
        this.message += name + "\n";
        this.message += "This is forbidden by JediScript style guide.";
    }
};
});
