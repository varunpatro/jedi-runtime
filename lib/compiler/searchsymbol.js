define(['lib/parser/nodes', 'lib/compiler/util'],
function(Nodes, Util) {

// Record all defined symbols in a function scope
function search_defined_symbols(stmt) {
	return search_defined_symbols_in_sequence(stmt).values;
}

function search_defined_symbols_in_sequence(stmt) {
	if (Nodes.empty_stmt(stmt)) {

		return Util.make_hash_set();

	} else if (Nodes.is_if_statement(Nodes.first_stmt(stmt))) {

		return Util.merge_hash_set([

			search_defined_symbols_in_if_statement(Nodes.first_stmt(stmt)),

			search_defined_symbols_in_sequence(Nodes.rest_stmts(stmt))]);

	} else if (Nodes.is_while_statement(Nodes.first_stmt(stmt))) {

		return Util.merge_hash_set([

			search_defined_symbols_in_while_statement(Nodes.first_stmt(stmt)),

			search_defined_symbols_in_sequence(Nodes.rest_stmts(stmt))]);

	} else if (Nodes.is_for_statement(Nodes.first_stmt(stmt))) {

		return Util.merge_hash_set([

			search_defined_symbols_in_for_statement(Nodes.first_stmt(stmt)),

			search_defined_symbols_in_sequence(Nodes.rest_stmts(stmt))]);

	} else if (Nodes.is_var_definition(Nodes.first_stmt(stmt))) {

		return Util.merge_hash_set([

			search_defined_symbols_in_var_definition(Nodes.first_stmt(stmt)),

			search_defined_symbols_in_sequence(Nodes.rest_stmts(stmt))]);

	} else {

		return search_defined_symbols_in_sequence(Nodes.rest_stmts(stmt));

	}
}

function search_defined_symbols_in_if_statement(stmt) {
	return Util.merge_hash_set([
			search_defined_symbols_in_if_statement_consequent(
				Nodes.if_consequent(stmt)),
			search_defined_symbols_in_if_statement_alternative(
				Nodes.if_alternative(stmt))]);
}

function search_defined_symbols_in_if_statement_consequent(stmt) {
	return search_defined_symbols_in_sequence(stmt);
}

function search_defined_symbols_in_if_statement_alternative(stmt) {
	return search_defined_symbols_in_sequence(stmt);
}

function search_defined_symbols_in_while_statement(stmt) {
	return search_defined_symbols_in_sequence(Nodes.while_statements(stmt));
}

function search_defined_symbols_in_while_statement_body(stmt) {
	return search_defined_symbols_in_sequence(stmt);
}

function search_defined_symbols_in_for_statement(stmt) {
	return Util.merge_hash_set([ search_defined_symbols_in_for_statement_initialiser(
				Nodes.for_initialiser(stmt)),
			search_defined_symbols_in_for_statement_body(
				Nodes.for_statements(stmt))]);
}

function search_defined_symbols_in_for_statement_initialiser(stmt) {
	if (Nodes.is_var_definition(stmt)) {
		return search_defined_symbols_in_var_definition(stmt);
	} else {
		return Util.make_hash_set();
	}
}

function search_defined_symbols_in_for_statement_body(stmt) {
	return search_defined_symbols_in_sequence(stmt);
}

function search_defined_symbols_in_var_definition(stmt) {
	var symbol_list = Util.make_hash_set({
		value: Nodes.var_definition_variable(stmt),
		line: stmt.line || 0
	});

	return symbol_list;
}

return {
	search_defined_symbols: search_defined_symbols
};

});
