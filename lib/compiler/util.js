define(function() {
'use strict';

function CompileError(message, line) {
	this.message = message;
	this.line = line;
}

function make_hash_set(value) {
	var hash_set = {
		values: [],
		table: {}
	};
	if (value && value.line !== undefined) {
		hash_set.values.push(value);
		hash_set.table[value.value] = true;
	} else if (value) {
		hash_set.values.push(value);
		hash_set.table[value] = true;
	}
	return hash_set;
}

function add_to_hash_set(hash_set, value) {
	var hash = variable_name_hash(value);
	if (value && value.line !== undefined) {
			hash = variable_name_hash(value.value);
	}
	if (!hash_set.table[hash]) {
		hash_set.table[hash] = true;
		hash_set.values.push(value);
	}
}

function merge_hash_set(hash_sets) {
	var new_hash_set = make_hash_set();

	var i;
	var end;
	function func_add_to_hash_set(value) {
		add_to_hash_set(new_hash_set, value);
	}
	for (i = 0, end = hash_sets.length; i < end; ++i) {
		hash_sets[i].values.forEach(func_add_to_hash_set);
	}

	return new_hash_set;
}

function make_compile_result(instructions, debug, parsed_functions, constant, constant_value) {
	return {
		instructions: instructions || [],
		debug: debug || [],
		parsed_functions: parsed_functions || [],
		constant: constant,
		constant_value: constant_value
	};
}

function make_debug_info(line) {
	return {
		line: line,
		function_name: undefined
	};
}

function merge_compile_result(compile_results) {
	var compile_result = make_compile_result();
	for(var i = 0, end = compile_results.length; i < end; ++i) {
		var source = compile_results[i];
		Array.prototype.push.apply(compile_result.instructions, source.instructions);
		Array.prototype.push.apply(compile_result.debug, source.debug);
		Array.prototype.push.apply(compile_result.parsed_functions, source.parsed_functions);
	}
	return compile_result;
}

var temporary_variable_index = 0;
function make_temporary_variable_name() {
	return ':' + temporary_variable_index++;
}

function reset_temporary_variable_name_counter() {
	temporary_variable_index = 0;
}

function variable_name_hash(name) {
	return 'var#' + name + '#';
}

function make_single_instruction(instruction, line_number, constant, constant_value) {
	return make_compile_result([instruction], [make_debug_info(line_number)], undefined, constant, constant_value);
}

function make_function_description(name, source) {
	return {
		name: name,
		compile_result: null,
		parameters: ['this'],
		source: source
	};
}

return {
	CompileError: CompileError,
	make_compile_result: make_compile_result,
	make_debug_info: make_debug_info,
	merge_compile_result: merge_compile_result,
	make_temporary_variable_name: make_temporary_variable_name,
	reset_temporary_variable_name_counter: reset_temporary_variable_name_counter,
	variable_name_hash: variable_name_hash,
	make_single_instruction: make_single_instruction,
	make_function_description: make_function_description,
	make_hash_set: make_hash_set,
	merge_hash_set: merge_hash_set,
	add_to_hash_set: add_to_hash_set
};

});
