define(function() {

function is_label(element) {
	return element.label;
}

function is_address_label(element) {
	return is_label(element) && element.address_label;
}

var label_count = 0;

function make_address_label() {
	return {
		name: 'label#' + label_count++,
		tag: 'label',
		label: true,
		address_label: true
	};
}

function reset_address_label() {
	label_count = 0;
}

function is_goto_label(element) {
	return is_label(element) && element.tag === 'goto_label';
}

function make_goto_label(label) {
	return {
		name: label_name(label),
		tag: 'goto_label',
		label: true
	};
}

function is_false_goto_label(element) {
	return is_label(element) && element.tag === 'false_goto_label';
}

function make_false_goto_label(label) {
	return {
		name: label_name(label),
		tag: 'false_goto_label',
		label: true
	};
}

function label_name(element) {
	return element.name;
}

function is_break_marker(element) {
	return element.tag === 'break_marker';
}

function make_break_marker() {
	return {
		tag: 'break_marker'
	};
}

function is_continue_marker(element) {
	return element.tag === 'continue_marker';
}

function make_continue_marker() {
	return {
		tag: 'continue_marker'
	};
}

function is_function_load_label(element) {
	return is_label(element) && element.tag === 'function_load_label';
}

function function_load_label_description(element) {
	return element.description;
}

function make_function_load_label(label, description) {
	return {
		name: label_name(label),
		source: description.source,
		description: description,
		tag: 'function_load_label',
		label: true
	};
}

function is_call_label(element) {
	return is_label(element) && element.tag === 'call_label';
}

function make_call_label(count) {
	return {
		tag: 'call_label',
		count: count,
		label: true
	};
}

function call_label_argument_length(element) {
	return element.count;
}

function is_return_label(element) {
	return is_label(element) && element.tag === 'return_label';
}

function make_return_label() {
	return {
		tag: 'return_label',
		label: true
	};
}

function is_load_temporary_variable_label(element) {
	return is_label(element) && element.tag === 'load_temporary_variable';
}

function make_load_temporary_variable_label(name) {
	return {
		label: true,
		tag: 'load_temporary_variable',
		name: name
	};
}

function is_store_temporary_variable_label(element) {
	return is_label(element) && element.tag === 'store_temporary_variable';
}

function make_store_temporary_variable_label(name) {
	return {
		label: true,
		tag: 'store_temporary_variable',
		name: name
	};
}

return {
	is_label: is_label,
	is_address_label: is_address_label,
	make_address_label: make_address_label,
	reset_address_label: reset_address_label,
	is_goto_label: is_goto_label,
	make_goto_label: make_goto_label,
	is_false_goto_label: is_false_goto_label,
	make_false_goto_label: make_false_goto_label,
	label_name: label_name,
	is_break_marker: is_break_marker,
	make_break_marker: make_break_marker,
	is_continue_marker: is_continue_marker,
	make_continue_marker: make_continue_marker,
	is_function_load_label: is_function_load_label,
	function_load_label_description: function_load_label_description,
	make_function_load_label: make_function_load_label,
	is_call_label: is_call_label,
	make_call_label: make_call_label,
	call_label_argument_length: call_label_argument_length,
	is_return_label: is_return_label,
	make_return_label: make_return_label,
	is_store_temporary_variable_label: is_store_temporary_variable_label,
	make_store_temporary_variable_label: make_store_temporary_variable_label,
	is_load_temporary_variable_label: is_load_temporary_variable_label,
	make_load_temporary_variable_label: make_load_temporary_variable_label
};

});
