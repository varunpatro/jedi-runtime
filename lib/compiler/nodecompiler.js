define([
	'lib/vm/instruction',
	'lib/parser/nodes',
	'lib/compiler/exception',
	'lib/compiler/util',
	'lib/compiler/instructionfactory',
	'lib/compiler/pseudoinstruction',
	'lib/compiler/searchsymbol'],
function(
	Instruction,
	Nodes,
	Exception,
	Util,
	InstructionFactory,
	PseudoInstruction,
	SearchSymbol){
'use strict';

var intermediate_value_variable_name = ':$';

function reset_all_counters() {
	Util.reset_temporary_variable_name_counter();
	PseudoInstruction.reset_address_label();
}

// Replace all loop control markers
function replace_loop_jumps(instructions, break_marker, continue_marker) {
	for(var i = 0, end = instructions.length; i < end; ++i) {
		if (PseudoInstruction.is_break_marker(instructions[i])) {
			instructions[i] = PseudoInstruction.make_goto_label(break_marker);
		} else if (PseudoInstruction.is_continue_marker(instructions[i])) {
			instructions[i] = PseudoInstruction.make_goto_label(continue_marker);
		}
	}
}

// Search all defined symbols and return a compile result declaring them
function compile_defined_symbols(stmt, source) {
	var symbol_list = SearchSymbol.search_defined_symbols(stmt);
	return Util.merge_compile_result(
			symbol_list.map(function(symbol){
				return Util.make_single_instruction(
					InstructionFactory.make_declare_symbol_instruction(symbol.value),
					symbol.line);
			}));
}

// Primtive Value
function compile_self_evaluating(stmt){
	return Util.make_single_instruction(
				InstructionFactory.make_load_constant_value_instruction(stmt),
				undefined,
				true,
				stmt
			);
}

// Variable
function compile_variable(stmt) {
	var var_name = Nodes.variable_name(stmt);
	return Util.make_single_instruction(
			InstructionFactory.make_load_symbol_instruction(var_name),
			Nodes.stmt_line(stmt)
		);
}

// Variable Definition
function compile_var_definition(stmt, source) {
	var var_name = Nodes.var_definition_variable(stmt);

	var var_value = Nodes.var_definition_value(stmt);
	var value_compile_result = compile_any_expression(var_value, source);
	var line_number = Nodes.stmt_line(stmt);

	// completion of line number, especially in case of primitive values
	if (value_compile_result.debug.length && !value_compile_result.debug[0].line)
		value_compile_result.debug[0].line = line_number;

	return Util.merge_compile_result([
			value_compile_result,
			Util.make_single_instruction(
				InstructionFactory.make_store_symbol_instruction(var_name),
				line_number),
			Util.make_single_instruction(
				InstructionFactory.make_load_constant_value_instruction())
		]);
}

// Assignment
function compile_assignment(stmt, source) {
	var var_name = Nodes.variable_name(Nodes.assignment_variable(stmt));

	var var_value = Nodes.assignment_value(stmt);
	var value_compile_result = compile_any_expression(var_value, source);
	// completion of line number, especially in case of primitive values
	if (value_compile_result.debug.length && !value_compile_result.debug[0].line)
		value_compile_result.debug[0].line = line_number;

	var line_number = Nodes.stmt_line(stmt);
		// We would like to keep the value after assignment
	return Util.merge_compile_result([
			value_compile_result,
			Util.make_single_instruction(
				PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name),
				line_number),
			Util.make_single_instruction(
				PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name)),
			Util.make_single_instruction(
				InstructionFactory.make_store_symbol_instruction(var_name)),
			Util.make_single_instruction(
				PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name))
		]);
}

// === Control Flows ===
function compile_if_statement(stmt, source) {
	var predicate_expression = Nodes.if_predicate(stmt);
	var predicate_compile_result = compile_any_expression(predicate_expression, source);

	var predicate_fail = PseudoInstruction.make_address_label();

	var consequent_compile_result = nodes.sequence.compile(Nodes.if_consequent(stmt), source);
	var alternative_compile_result = nodes.sequence.compile(Nodes.if_alternative(stmt), source);

	var consequent_exit = PseudoInstruction.make_address_label();
	return Util.merge_compile_result([
			predicate_compile_result,
			Util.make_single_instruction(PseudoInstruction.make_false_goto_label(predicate_fail)),
			consequent_compile_result,
			Util.make_single_instruction(PseudoInstruction.make_goto_label(consequent_exit)),
			Util.make_single_instruction(predicate_fail),
			alternative_compile_result,
			Util.make_single_instruction(consequent_exit)
		]);
}

function compile_ternary_statement(stmt, source) {
	var predicate_expression = Nodes.ternary_predicate(stmt);
	var predicate_compile_result = compile_any_expression(predicate_expression, source);

	var predicate_fail = PseudoInstruction.make_address_label();

	var consequent_expression = Nodes.ternary_consequent(stmt);
	var alternative_expression = Nodes.ternary_alternative(stmt);
	var consequent_compile_result = compile_any_expression(consequent_expression, source);
	var alternative_compile_result = compile_any_expression(alternative_expression, source);

	var consequent_exit = PseudoInstruction.make_address_label();
	return Util.merge_compile_result([
		predicate_compile_result,
		Util.make_single_instruction(PseudoInstruction.make_false_goto_label(predicate_fail)),
		consequent_compile_result,
		Util.make_single_instruction(PseudoInstruction.make_goto_label(consequent_exit)),
		Util.make_single_instruction(predicate_fail),
		alternative_compile_result,
		Util.make_single_instruction(consequent_exit)
		]);
}

function compile_while_statement(stmt, source) {
	var predicate_test = PseudoInstruction.make_address_label();
	var predicate_expression = Nodes.while_predicate(stmt);
	var predicate_compile_result = compile_any_expression(predicate_expression, source);
	var loop_terminate = PseudoInstruction.make_address_label();

	var body_compile_result = nodes.sequence.compile(Nodes.while_statements(stmt), source);
	replace_loop_jumps(body_compile_result.instructions, loop_terminate, predicate_test);

	return Util.merge_compile_result([
		Util.make_single_instruction(predicate_test),
		predicate_compile_result,
		Util.make_single_instruction(PseudoInstruction.make_false_goto_label(loop_terminate)),

		body_compile_result,
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),

		Util.make_single_instruction(
			PseudoInstruction.make_goto_label(predicate_test)),

		Util.make_single_instruction(loop_terminate),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name))
	]);
}

function compile_for_statement(stmt, source) {
	var initialiser_expression = Nodes.for_initialiser(stmt);
	var initialiser_compile_result = compile_any_expression(initialiser_expression, source);

	var predicate_test = PseudoInstruction.make_address_label();
	var predicate_expression = Nodes.for_predicate(stmt);
	var predicate_compile_result = compile_any_expression(predicate_expression, source);
	// body statements
	var loop_terminate = PseudoInstruction.make_address_label();
	var finaliser_routine = PseudoInstruction.make_address_label();
	var body_compile_result = nodes.sequence.compile(Nodes.for_statements(stmt), source);
	replace_loop_jumps(body_compile_result, loop_terminate, finaliser_routine);	// replace all loop controls
	// finaliser routine
	var finaliser_expression = Nodes.for_finaliser(stmt);
	var finaliser_compile_result = compile_any_expression(finaliser_expression, source);

	return Util.merge_compile_result([
		initialiser_compile_result,
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),

		Util.make_single_instruction(predicate_test),
		predicate_compile_result,
		Util.make_single_instruction(PseudoInstruction.make_false_goto_label(loop_terminate)),

		body_compile_result,
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
		Util.make_single_instruction(finaliser_routine),

		finaliser_compile_result,

		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
		Util.make_single_instruction(PseudoInstruction.make_goto_label(predicate_test)),

		Util.make_single_instruction(loop_terminate),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name))
	]);
}

function compile_break_statement(stmt) {
	return Util.make_single_instruction(
				PseudoInstruction.make_break_marker(),
				Nodes.stmt_line(stmt));
}

function compile_continue_statement(stmt) {
	return Util.make_single_instruction(
				PseudoInstruction.make_continue_marker(),
				Nodes.stmt_line(stmt));
}

function compile_sequence(stmt, source) {
	if (Nodes.empty_stmt(stmt)) {
		return Util.merge_compile_result([
			Util.make_single_instruction(
				InstructionFactory.make_load_constant_value_instruction(),
				Nodes.stmt_line(stmt)),

			Util.make_single_instruction(
				PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),

			Util.make_single_instruction(
				InstructionFactory.make_load_constant_value_instruction(),
				Nodes.stmt_line(stmt))
		]);
	}
	var statements = [];
	while(!Nodes.empty_stmt(stmt)) {
		var first = Nodes.first_stmt(stmt);
		stmt = Nodes.rest_stmts(stmt);
		statements.push(
			compile_any_expression(first, source),
			Util.make_single_instruction(
				PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)));
	}
	statements.push(
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name)));

	return Util.merge_compile_result(statements);
}

function compile_boolean_operation_and(stmt, source) {
	var and_operands = Nodes.operands(stmt);
	var first = Nodes.first_operand(and_operands);
	var second = Nodes.first_operand(Nodes.rest_stmts(and_operands));
	var line_number = Nodes.stmt_line(stmt);

	var temp_var_name = Util.make_temporary_variable_name();

	var short_circuit = PseudoInstruction.make_address_label();
	var evaluation_terminal = PseudoInstruction.make_address_label();

	return Util.merge_compile_result([
		compile_any_expression(first, source),
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(temp_var_name),
			line_number),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(temp_var_name)),
		Util.make_single_instruction(
			PseudoInstruction.make_false_goto_label(short_circuit)),

		compile_any_expression(second, source),
		Util.make_single_instruction(
			PseudoInstruction.make_goto_label(evaluation_terminal)),

		Util.make_single_instruction(short_circuit),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(temp_var_name),
			line_number),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(temp_var_name)),
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),

		Util.make_single_instruction(evaluation_terminal)
	]);
}

function compile_boolean_operation_or(stmt, source) {
	var or_operands = Nodes.operands(stmt);
	var first = Nodes.first_operand(or_operands);
	var second = Nodes.first_operand(Nodes.rest_stmts(or_operands));

	var compile_results = [];
	var evaluation_terminal = PseudoInstruction.make_address_label();
	var false_continue_evaluation = PseudoInstruction.make_address_label();
	var temp_var_name = Util.make_temporary_variable_name();
	return Util.merge_compile_result([
			compile_any_expression(first, source),

			Util.make_single_instruction(
				PseudoInstruction.make_store_temporary_variable_label(temp_var_name),
				Nodes.stmt_line(stmt)),
			Util.make_single_instruction(
				PseudoInstruction.make_load_temporary_variable_label(temp_var_name)),
			Util.make_single_instruction(
				PseudoInstruction.make_false_goto_label(false_continue_evaluation)),

			Util.make_single_instruction(
				PseudoInstruction.make_load_temporary_variable_label(temp_var_name)),
			Util.make_single_instruction(
				PseudoInstruction.make_load_temporary_variable_label(temp_var_name)),
			Util.make_single_instruction(
				PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
			Util.make_single_instruction(
				PseudoInstruction.make_goto_label(evaluation_terminal)),

			Util.make_single_instruction(false_continue_evaluation),
			compile_any_expression(second, source),
			Util.make_single_instruction(evaluation_terminal)
		]);
}

function compile_boolean_operation(stmt, source) {
	switch(Nodes.operator(stmt)) {
	case '&&':
		return compile_boolean_operation_and(stmt, source);
	case '||':
		return compile_boolean_operation_or(stmt, source);
	default:
		throw new Util.CompileError('Unexpected boolean operation operator');
	}
}

// === Function definition and Application ===
function compile_function_definition(stmt, source) {
	function get_input_text(location) {
		return source.substring(location.start_offset, location.end_offset);
	}

	var location = Nodes.function_definition_text_location(stmt);
	var function_description = Util.make_function_description(Nodes.function_definition_name(stmt),
		get_input_text(location));
	for (
		var parameters = Nodes.function_definition_parameters(stmt);
		!Nodes.no_operands(parameters);
		parameters = Nodes.rest_operands(parameters)) {

		var parameter_name = Nodes.first_operand(parameters);
		function_description.parameters.push(parameter_name);
	}

	// A label is used here so that load instruction can refer to this label later
	var function_definition = PseudoInstruction.make_address_label();
	var function_body_statements = Nodes.function_definition_body(stmt);
	var body_compile_result = nodes.sequence.compile(function_body_statements, source);
	var defined_symbols = compile_defined_symbols(function_body_statements, source);

	function_description.compile_result = Util.merge_compile_result([
		Util.make_single_instruction(function_definition),	// Address label for start of function

		defined_symbols,
		body_compile_result,	// Function body statements

		Util.make_single_instruction(
			InstructionFactory.make_load_constant_value_instruction()),
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
		Util.make_single_instruction(
			InstructionFactory.make_load_constant_value_instruction()),	// Default return value
		Util.make_single_instruction(PseudoInstruction.make_return_label())	// Return label
		]);
	var compile_result = Util.make_single_instruction(
		PseudoInstruction.make_function_load_label(function_definition, function_description),
		Nodes.stmt_line(stmt));
	compile_result.parsed_functions.push(function_description);
	// Make sure function parameters are not defined
	defined_symbols.instructions.forEach(
		function(instruction, index) {
				if (function_description.parameters.indexOf(instruction.value) > -1) {
			      throw new Exception.ClosureFormalRedeclarationException(
							instruction.value, defined_symbols.debug[index].line);
				}
	});
	return compile_result;
}

var primitive_operator_instruction_map = {
	'+': new Instruction('PLUS'),
	'-': new Instruction('SUB'),
	'*': new Instruction('TIMES'),
	'/': new Instruction('DIV'),
	'%': new Instruction('MOD'),
	'>=': new Instruction('GREATERTHANEQ'),
	'<=': new Instruction('LESSTHANEQ'),
	'>': new Instruction('GREATERTHAN'),
	'<': new Instruction('LESSTHAN'),
	'===': new Instruction('EQUAL'),
	'!==': new Instruction('NOTEQUAL'),
	'!': new Instruction('BNEG'),
	'arithmetic-negation': new Instruction('ANEG')
};

function compile_application(stmt, source) {
	var parameters = Nodes.operands(stmt);
	var first;
	var second;
	var compile_result;
	var operator_instruction;
	var is_primitive =
		Nodes.is_variable(Nodes.operator(stmt)) &&
		primitive_operator_instruction_map.hasOwnProperty(Nodes.variable_name(Nodes.operator(stmt)));
	var line_number = Nodes.stmt_line(stmt);

	if(is_primitive) {
		var op_name = Nodes.variable_name(Nodes.operator(stmt));
		if (op_name === '+' &&	// The operator is +
			Nodes.no_operands(Nodes.rest_operands(parameters))) {	// Unary operator +

			return compile_any_expression(Nodes.first_operand(parameters), source);

		} else if (op_name === '-' &&	// The operator is -
			Nodes.no_operands(Nodes.rest_operands(parameters))) {	// Unary operator - (negative)

			return Util.merge_compile_result([
				compile_any_expression(Nodes.first_operand(parameters), source),
				Util.make_single_instruction(
					primitive_operator_instruction_map['arithmetic-negation'],
					line_number)
			]);

		} else if (op_name === '!') {	// Boolean not operator

			return Util.merge_compile_result([
				compile_any_expression(Nodes.first_operand(parameters), source),
				Util.make_single_instruction(primitive_operator_instruction_map['!'], line_number)
			]);

		} else {

			return Util.merge_compile_result([
				compile_any_expression(
					Nodes.first_operand(parameters), source),
				compile_any_expression(
					Nodes.first_operand(
						Nodes.rest_operands(parameters)), source),
				Util.make_single_instruction(
					primitive_operator_instruction_map[op_name],
					line_number)]);
		}
	} else {
		var application_operator = Nodes.operator(stmt);
		var compile_results = [
			compile_any_expression(application_operator, source),
			Util.make_single_instruction(
				InstructionFactory.make_load_constant_value_instruction(),
				line_number)
		];
		var count;
		for(parameters = Nodes.operands(stmt), count = 1;
			!Nodes.no_operands(parameters);
			parameters = Nodes.rest_operands(parameters), ++count){
			first = Nodes.first_operand(parameters);
			compile_results.push(compile_any_expression(first, source));
		}
		compile_results.push(
			Util.make_single_instruction(
				PseudoInstruction.make_call_label(count)));

		return Util.merge_compile_result(compile_results);
	}
}

function compile_return_statement(stmt, source) {
	var return_value = Nodes.return_statement_expression(stmt);

	return Util.merge_compile_result([
		compile_any_expression(return_value, source),
		Util.make_single_instruction(
			PseudoInstruction.make_return_label(),
			Nodes.stmt_line(stmt))
		]);
}

// === Record operations ===
function compile_object_method_application(stmt, source) {
	var object_expression = Nodes.object(stmt);
	var temp_var_name = Util.make_temporary_variable_name();
	var property_key_expression = Nodes.object_property(stmt);
	var line_number = Nodes.stmt_line(stmt);
	var compile_results = [
		compile_any_expression(object_expression, source),

		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(temp_var_name),
			line_number),

		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(temp_var_name)),

		compile_any_expression(property_key_expression, source),

		Util.make_single_instruction(
			InstructionFactory.make_read_object_property_instruction(),
			line_number),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(temp_var_name))
	];

	var count;
	var method_arguments;
	for(method_arguments = Nodes.operands(stmt), count = 1;
		!Nodes.no_operands(method_arguments);
		method_arguments = Nodes.rest_operands(method_arguments), ++count) {
		var first = Nodes.first_operand(method_arguments);
		compile_results.push(compile_any_expression(first, source));
	}

	compile_results.push(
		Util.make_single_instruction(
			PseudoInstruction.make_call_label(count), line_number),
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(
				intermediate_value_variable_name)),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(
				intermediate_value_variable_name)));

	return Util.merge_compile_result(compile_results);
}

function compile_object_expression(stmt, source) {
	var pairs;
	var count;
	var compile_results = [];

	for(pairs = Nodes.object_expression_pairs(stmt), count = 0;
		!Nodes.empty_object_expression_pairs(pairs);
		pairs = Nodes.rest_object_expression_pairs(pairs), ++count){

		var current_pair = Nodes.first_object_expression_pair(pairs);
		var key = Nodes.object_expression_pair_key(current_pair);
		var value = Nodes.object_expression_pair_value(current_pair);
		compile_results.push(
			compile_any_expression(value, source),
			Util.make_single_instruction(
				InstructionFactory.make_load_constant_value_instruction(key),
				Nodes.stmt_line(current_pair)));
		}

	compile_results.push(
		Util.make_single_instruction(
			InstructionFactory.make_load_literal_object_instruction(count),
			Nodes.stmt_line(stmt)),
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name)));

	return Util.merge_compile_result(compile_results);
}

function compile_empty_list_expression(stmt) {
    var compile_results = [];
    compile_results.push(
        Util.make_single_instruction(InstructionFactory.make_load_literal_array_instruction(0),
                                     Nodes.stmt_line(stmt)),
        Util.make_single_instruction(
            PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
        Util.make_single_instruction(
            PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name)));
    return Util.merge_compile_result(compile_results);
}

function compile_array_expression(stmt, source) {
	var elements;
	var count;
	var compile_results = [];
	for(elements = Nodes.array_expression_elements(stmt), count = 0;
		!Nodes.empty_array_element(elements);
		elements = Nodes.rest_array_elements(elements), ++count) {

		var element = Nodes.first_array_element(elements);
		compile_results.push(compile_any_expression(element, source));

	}
	compile_results.push(
		Util.make_single_instruction(InstructionFactory.make_load_literal_array_instruction(count), Nodes.stmt_line(stmt)),
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name)));

	return Util.merge_compile_result(compile_results);
}

function compile_construction_expression(stmt, source) {
	var temp_var_name = Util.make_temporary_variable_name();
	var line_number = Nodes.stmt_line(stmt);
	var compile_results = [
		Util.make_single_instruction(InstructionFactory.make_load_symbol_instruction(Nodes.construction_type(stmt)), line_number),
		Util.make_single_instruction(InstructionFactory.make_load_literal_object_instruction(0))
	];

	var method_arguments;
	var count;
	for(method_arguments = Nodes.operands(stmt), count = 1;
		!Nodes.no_operands(method_arguments);
		method_arguments = Nodes.rest_operands(method_arguments), ++count) {

		var first = Nodes.first_operand(method_arguments);
		compile_results.push(compile_any_expression(first, source));

	}
	compile_results.push(
		Util.make_single_instruction(InstructionFactory.make_construction_call_instruction(count), line_number),
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name)));

	return Util.merge_compile_result(compile_results);
}

function compile_property_access(stmt, source) {
	var object_expression = Nodes.property_access_object(stmt);
	var object_property = Nodes.property_access_property(stmt);
	var compile_results = [
		compile_any_expression(object_expression, source),
		compile_any_expression(object_property, source)
	];
	compile_results.push(
		Util.make_single_instruction(InstructionFactory.make_read_object_property_instruction(), Nodes.stmt_line(stmt)),
		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name)));

	return Util.merge_compile_result(compile_results);
}

function compile_property_assignment(stmt, source) {
	var object_expression = Nodes.property_assignment_object(stmt);
	var object_property = Nodes.property_assignment_property(stmt);
	var value_expression = Nodes.property_assignment_value(stmt);
	var temp_var_name = Util.make_temporary_variable_name();

	return Util.merge_compile_result([
		compile_any_expression(object_expression, source),
		compile_any_expression(object_property, source),
		compile_any_expression(value_expression, source),

		Util.make_single_instruction(
			PseudoInstruction.make_store_temporary_variable_label(intermediate_value_variable_name)),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name)),
		Util.make_single_instruction(InstructionFactory.make_store_object_property_instruction(), Nodes.stmt_line(stmt)),
		Util.make_single_instruction(
			PseudoInstruction.make_load_temporary_variable_label(intermediate_value_variable_name))
		]);
}

// === Any ===
function compile_any_expression(stmt, source){
	for(var i = 0, end = node_types.length; i < end; ++i) {
		var node = nodes[node_types[i]];
		if (node.is(stmt)) {
			return node.compile(stmt, source);
		}
	}
	throw new Util.CompileError('Unexpected statement type', stmt);
}

var nodes = {
	'self-evaluating': {is: Nodes.is_self_evaluating, compile: compile_self_evaluating},
	'variable': {is: Nodes.is_variable, compile: compile_variable},
	'var-definition': {is: Nodes.is_var_definition, compile: compile_var_definition},
	'assignment': {is: Nodes.is_assignment, compile: compile_assignment},
	'if': {is: Nodes.is_if_statement, compile: compile_if_statement},
	'ternary': {is: Nodes.is_ternary_statement, compile: compile_ternary_statement},
	'while': {is: Nodes.is_while_statement, compile: compile_while_statement},
	'for': {is: Nodes.is_for_statement, compile: compile_for_statement},
	'break': {is: Nodes.is_break_statement, compile: compile_break_statement},
	'continue': {is: Nodes.is_continue_statement, compile: compile_continue_statement},
	'function-definition': {is: Nodes.is_function_definition, compile: compile_function_definition},
	'sequence': {is: Nodes.is_sequence, compile: compile_sequence},
	'boolean-op': {is: Nodes.is_boolean_operation, compile: compile_boolean_operation},
	'application': {is: Nodes.is_application, compile: compile_application},
	'return': {is: Nodes.is_return_statement, compile: compile_return_statement},
	'object-expression': {is: Nodes.is_object_expression, compile: compile_object_expression},
	'object-method': {is: Nodes.is_object_method_application, compile: compile_object_method_application},
  'empty-list': {is:Nodes.is_empty_list_statement, compile: compile_empty_list_expression},
	'array-expression': {is: Nodes.is_array_expression, compile: compile_array_expression},
	'construction': {is: Nodes.is_construction, compile: compile_construction_expression},
	'property-access': {is: Nodes.is_property_access, compile: compile_property_access},
	'property-assignment': {is: Nodes.is_property_assignment, compile: compile_property_assignment}
};

var node_types = Object.getOwnPropertyNames(nodes);

// === Entry ===
function compile_main(parse_tree, source) {
	return Util.merge_compile_result([
			compile_defined_symbols(parse_tree, source),
			compile_sequence(parse_tree, source),
			Util.make_single_instruction(InstructionFactory.make_done_instruction())
		]);
}

return {
	compile_main: compile_main,
	compile_any_expression: compile_any_expression,
	reset_all_counters: reset_all_counters
};

});
