/**
* compiler.js
* requires instruction.js and parse.js
* TODO: switch to a new parse tree structure
*
*/
define(['lib/compiler/util', 'lib/compiler/nodecompiler', 'lib/compiler/preprocess', 'lib/compiler/postprocess'],
function(Util, NodeCompiler, Preprocess, Postprocess){
'use strict';

/**
* compiler 
*
* @param {Node} parse_tree The parse tree of the source
* @param {String} source The program source, for debug information generation
* @return {Array} an array of instruction for the virtual machine to interpret
*/
function compile(parse_tree, source) {
	NodeCompiler.reset_all_counters();

	Preprocess.check_semantics(parse_tree);
	var compile_results = [NodeCompiler.compile_main(parse_tree, source)];

	var parsed_functions = [];
	Array.prototype.push.apply(parsed_functions, compile_results[0].parsed_functions);

	var function_description;
	function replace_function_name(debug_info) {
		debug_info.function_name = function_description.name;
	}

	while(parsed_functions.length) {
		function_description = parsed_functions.shift();
		// Fill function name
		function_description.compile_result.debug.forEach(replace_function_name);

		compile_results.push(function_description.compile_result);
		Array.prototype.push.apply(parsed_functions, function_description.compile_result.parsed_functions);
		function_description.compile_result.parsed_functions.length = 0;	// do not leave them
	}

	var compile_result = Util.merge_compile_result(compile_results);

	// Fill the missing line numbers (implied from the previous instruction)
	if (compile_result.debug[0].line === undefined) {
		compile_result.debug[0].line = 0;
	}
	for (var i = 1, end = compile_result.debug.length; i < end; ++i) {
		if (compile_result.debug[i].line === undefined) {
			compile_result.debug[i].line = compile_result.debug[i - 1].line;
		}
	}

	// Set function names
	return Postprocess.post_process(compile_result);
}


return compile;

});
