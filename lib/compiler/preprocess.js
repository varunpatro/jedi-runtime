define(['lib/parser/nodes', 'lib/compiler/util'],
function(Nodes, Util) {

function make_check_info() {
	return {
		loop_controls: [],
		returns: []
	};
}

function merge_check_info(check_infos) {
	var check_info = make_check_info();
	for(var i = 0, end = check_infos.length; i < end; ++i) {
		Array.prototype.push.apply(check_info.loop_controls, check_infos[i].loop_controls);
		Array.prototype.push.apply(check_info.returns, check_infos[i].returns);
	}
	return check_info;
}

function check_variable(stmt) {
	if (Nodes.variable_name(stmt) === '__proto__') {
		// TODO: Make this not an error
		throw new Util.CompileError('Unexpected token __proto__', Nodes.stmt_line(stmt));
	}
	return make_check_info();
}

function check_var_definition(stmt) {
	if (Nodes.var_definition_variable(stmt) === 'this') {
		throw new Util.CompileError('Unexpected token this', Nodes.stmt_line(stmt));
	}
	if (Nodes.variable_name(stmt) === '__proto__') {
		// TODO: Make this not an error
		throw new Util.CompileError('Unexpected token __proto__', Nodes.stmt_line(stmt));
	}
	return check_any_statement(Nodes.var_definition_value(stmt));
}

function check_assignment(stmt) {
	check_variable(Nodes.assignment_variable(stmt));
	if (Nodes.variable_name(stmt) === '__proto__') {
		// TODO: Make this not an error
		throw new Util.CompileError('Unexpected token __proto__', Nodes.stmt_line(stmt));
	}
	return check_any_statement(Nodes.assignment_value(stmt));
}

function check_if_statement(stmt) {
	return merge_check_info([
			check_any_statement(Nodes.if_predicate(stmt)),
			check_any_statement(Nodes.if_consequent(stmt)),
			check_any_statement(Nodes.if_alternative(stmt))
		]);
}

function check_ternary_statement(stmt) {
	return merge_check_info([
			check_any_statement(Nodes.ternary_predicate(stmt)),
			check_any_statement(Nodes.ternary_consequent(stmt)),
			check_any_statement(Nodes.ternary_alternative(stmt))
		]);

}

function check_while_statement(stmt) {
	var check_info = merge_check_info([
			check_any_statement(Nodes.while_predicate(stmt)),
			check_sequence(Nodes.while_statements(stmt))
		]);
	check_info.loop_controls.length = 0;
	return check_info;
}

function check_for_statement(stmt) {
	var check_info = merge_check_info([
			check_any_statement(Nodes.for_initialiser(stmt)),
			check_any_statement(Nodes.for_predicate(stmt)),
			check_any_statement(Nodes.for_statements(stmt)),
			check_any_statement(Nodes.for_finaliser(stmt))
		]);
	check_info.loop_controls.length = 0;
	return check_info;
}

function check_break_statement(stmt) {
	var check_info = make_check_info();
	check_info.loop_controls.push(Nodes.stmt_line(stmt));
	return check_info;
}

function check_continue_statement(stmt) {
	var check_info = make_check_info();
	check_info.loop_controls.push(Nodes.stmt_line(stmt));
	return check_info;
}

function check_function_definition(stmt) {
	var check_info = check_sequence(Nodes.function_definition_body(stmt));
	if (check_info.loop_controls.length) {
		throw new Util.CompileError('Invalid loop controls', check_info.loop_controls);
	}
	for(
		var parameters = Nodes.function_definition_parameters(stmt);
		!Nodes.no_operands(parameters);
		parameters = Nodes.rest_operands(parameters)) {
		if (Nodes.first_operand(parameters) === 'this') {
			throw new Util.CompileError('Unexpected token this', Nodes.stmt_line(stmt));
		}
		if (Nodes.first_operand(parameters) === '__proto__') {
			// TODO: Make this not an error
			throw new Util.CompileError('Unexpected token __proto__', Nodes.stmt_line(stmt));
		}
	}
	return make_check_info();
}

function check_return_statement(stmt) {
	return check_any_statement(Nodes.return_statement_expression(stmt));
}

function check_sequence(stmt) {
	var check_infos = [];
	while(!Nodes.empty_stmt(stmt)) {
		check_infos.push(check_any_statement(Nodes.first_stmt(stmt)));
		stmt = Nodes.rest_stmts(stmt);
	}
	return merge_check_info(check_infos);
}

function check_boolean_operation(stmt) {
	var parameters = Nodes.operands(stmt);
	return merge_check_info([
		check_any_statement(Nodes.first_operand(parameters)),
		check_any_statement(Nodes.first_operand(Nodes.rest_operands(parameters)))
		]);
}

function check_application(stmt) {
	var check_infos = [check_any_statement(Nodes.operator(stmt))];
	for(var parameters = Nodes.operands(stmt);
		!Nodes.no_operands(parameters);
		parameters = Nodes.rest_operands(parameters)) {
		check_infos.push(check_any_statement(Nodes.first_operand(parameters)));
	}
	return merge_check_info(check_infos);
}

function check_object_expression(stmt) {
	var check_infos = [];
	for(var pairs = Nodes.object_expression_pairs(stmt);
		!Nodes.empty_object_expression_pairs(pairs);
		pairs = Nodes.rest_object_expression_pairs(pairs)) {
		check_infos.push(
			check_any_statement(
				Nodes.object_expression_pair_value(
					Nodes.first_object_expression_pair(pairs))));
	}
	return merge_check_info(check_infos);
}

function check_object_method_application(stmt) {
	var check_infos = [check_any_statement(Nodes.object(stmt)), check_any_statement(Nodes.object_property(stmt))];
	for(var parameters = Nodes.operands(stmt);
		!Nodes.no_operands(parameters);
		parameters = Nodes.rest_operands(parameters)) {
		check_infos.push(check_any_statement(Nodes.first_operand(parameters)));
	}
	return merge_check_info(check_infos);
}

function check_array_expression(stmt) {
	var check_infos = [];
	for (var elements = Nodes.array_expression_elements(stmt);
		!Nodes.empty_array_element(elements);
		elements = Nodes.rest_array_elements(elements)) {
		check_infos.push(check_any_statement(Nodes.first_array_element(elements)));
	}
	return merge_check_info(check_infos);
}

function check_construction_expression(stmt) {
	var check_infos = [];
	if (Nodes.construction_type(stmt) === '__proto__') {
		// TODO: Make this not an error
		throw new Util.CompileError('Unexpected token __proto__', Nodes.stmt_line(stmt));
	}
	for(var parameters = Nodes.operands(stmt);
		!Nodes.no_operands(parameters);
		parameters = Nodes.rest_operands(parameters)) {
		check_infos.push(check_any_statement(Nodes.first_operand(parameters)));
	}
	return merge_check_info(check_infos);
}

function check_property_access(stmt) {
	return merge_check_info([
			check_any_statement(Nodes.property_access_object(stmt)),
			check_any_statement(Nodes.property_access_property(stmt))
		]);
}

function check_property_assignment(stmt) {
	return merge_check_info([
			check_any_statement(Nodes.property_assignment_object(stmt)),
			check_any_statement(Nodes.property_assignment_property(stmt)),
			check_any_statement(Nodes.property_assignment_value(stmt))
		]);
}

function check_any_statement(stmt) {
	if (Nodes.is_self_evaluating(stmt)) {
		return make_check_info();
	} else if (Nodes.is_variable(stmt)) {
		return check_variable(stmt);
	} else if (Nodes.is_var_definition(stmt)) {
		return check_var_definition(stmt);
	} else if (Nodes.is_assignment(stmt)) {
		return check_assignment(stmt);
	} else if (Nodes.is_if_statement(stmt)) {
		return check_if_statement(stmt);
	} else if (Nodes.is_ternary_statement(stmt)) {
		return check_ternary_statement(stmt);
	} else if (Nodes.is_while_statement(stmt)) {
		return check_while_statement(stmt);
	} else if (Nodes.is_for_statement(stmt)) {
		return check_for_statement(stmt);
	} else if (Nodes.is_break_statement(stmt)) {
		return check_break_statement(stmt);
	} else if (Nodes.is_continue_statement(stmt)) {
		return check_continue_statement(stmt);
	} else if (Nodes.is_function_definition(stmt)) {
		return check_function_definition(stmt);
	} else if (Nodes.is_sequence(stmt)) {
		return check_sequence(stmt);
	} else if (Nodes.is_boolean_operation(stmt)) {
		return check_boolean_operation(stmt);
	} else if (Nodes.is_application(stmt)) {
		return check_application(stmt);
	} else if (Nodes.is_return_statement(stmt)) {
		return check_return_statement(stmt);
	} else if (Nodes.is_object_expression(stmt)) {
		return check_object_expression(stmt);
	} else if (Nodes.is_object_method_application(stmt)) {
		return check_object_method_application(stmt);
	} else if (Nodes.is_array_expression(stmt)) {
		return check_array_expression(stmt);
	} else if (Nodes.is_construction(stmt)) {
		return check_construction_expression(stmt);
	} else if (Nodes.is_property_access(stmt)) {
		return check_property_access(stmt);
	} else if (Nodes.is_property_assignment(stmt)) {
		return check_property_assignment(stmt);
	} else if (Nodes.is_empty_list_statement(stmt)) {
    return true;
  } else {
		throw new Util.CompileError('Unexpected statement type', stmt);
	}
}

function check_semantics(stmt) {
	var check_info = check_sequence(stmt);
	if (check_info.loop_controls.length) {
		throw new Util.CompileError('Invalid loop controls', check_info.loop_controls);
	}
	if (check_info.returns.length) {
		throw new Util.CompileError('Returns are not allowed at top level', check_info.returns);
	}
}

return {
	check_semantics: check_semantics
};

});
