/**
 * Environment agnostic main entry point of the Jediscript Runtime.
 */

define("lib/main", function(require) {
    return {
        version: '0.1.22',
        Parser: function (week) {
            var Parser = require('lib/parser/parser-week-' + week + '.js');
            Parser.yy.parseError = Parser.parseError;
            return Parser;
        },
        Compiler: {
            compile: require('lib/compiler/compiler')
        },
        Runtime: require('lib/vm/runtime'),
        Debug: require('lib/debug/debug')
    };
});
