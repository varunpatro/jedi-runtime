/*globals define */
/** @module exception
 *  Defines the exception class for the debugger.
 *  @author Varun Patro <varun.kumar.patro@gmail.com>
 */
define([], function() {
    "use strict";

    function DebugException (message) {
        this.message = message;
    }

    return DebugException;
});