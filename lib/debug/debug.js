/** @module debug
 * Makes use of the compiler and vm to execute instructions
 * and facilitate debugging
 *  @author Varun Patro <varun.kumar.patro@gmail.com>
 */
define(['lib/parser/parser', 'lib/compiler/compiler', 'lib/vm/runtime',
        'lib/debug/debugException', 'lib/messageLibrary', 'lib/vm/internal/environment',
        'lib/vm/internal/closure'
    ],
    function(Parser, compile, Runtime, DebugException, Msg, Environment, Closure) {
        function Debug(source, symbol, context) {
            this.source = source;
            this.runtime = null;
            this.symbol = symbol;
            this.context = context;
            this.instructionArray = null;
            this.debugSymbols = null;
            this.lineNumbers = null;
            this.breakpoints = {};
            this.lastBreakpoint = undefined;
            this.status = Msg.VM.Response.INSTANCE_UNDEFINED;
            this.__init();
        }

        /**
         * initialize the Debug object by creating the instructionArray and 
         * debugSymbols object from the source code
         * @return {undefined} returns nothing
         */
        Debug.prototype.__init = function() {
            if (typeof this.source !== "string") {
                throw new DebugException("Source code is not a string.");
            }
            var compiledObject = compile(Parser.parse(this.source), this.source);
            this.instructionArray = compiledObject.instructions;
            this.debugSymbols = compiledObject.debug;
            this.lineNumbers = compiledObject.debug.map(function(instruction) {
                return instruction.line;
            });
            this.runtime = new Runtime(this.symbol, this.context);
        };

        /**
         * method to start the VM.
         * this method can be called to begin the VM execution cycle
         * as well as resume a paused VM execution cycle (paused by breakpoints)
         * @return {object} returns an object containing the top value of the operand stack 
         *                          and the status of the execution cycle (either suspended or finished)
         */
        Debug.prototype.start = function() {
            if (this.lastBreakpoint === undefined && this.breakpoints[0]) {
                this.lastBreakpoint = 0;
                return {
                    "value": null,
                    "status": Msg.VM.Response.INSTANCE_SET
                };
            }
            if (this.status === Msg.VM.Response.EXECUTION_FINISHED) {
                return {
                    "value": null,
                    "status": this.status
                };
            }
            var result = this.runtime.execute_instruction(this.instructionArray, this.lastBreakpoint, this.breakpoints);
            this.status = result.status;
            var runtimeInfo = this.runtime.get_runtime_information();
            this.lastBreakpoint = runtimeInfo.pc;
            var osValue = (runtimeInfo.operand_stack.length === 0) ? null :
                runtimeInfo.operand_stack[runtimeInfo.operand_stack.length - 1];
            return {
                "value": osValue,
                "status": result.status
            };
        };

        /**
         * method to set breakpoints in the VM execution cycle
         * @param {array} breakpointsArray array of line numbers (0 based)
         */
        Debug.prototype.setBreakpoints = function(breakpointsArray) {
            var breakpoints = {};
            for (var i = 0; i < breakpointsArray.length; i++) {
                var linenumber = breakpointsArray[i];
                var bp = this.__getFirstInstructionNumber(linenumber);
                breakpoints[bp] = true;
            }
            this.breakpoints = breakpoints;
        };

        /**
         * method to obtain the scope variables in the current frame and
         *     all parent frames.
         * @return {object} a nested object which contains variables and
         *                    functions in current and parent scopes.
         */
        Debug.prototype.getScopeVariables = function(env) {
            var environment = env || this.runtime.environment;
            var scopeVariables = {};
            scopeVariables.variables = this.__getVariablesInFrame(environment.__env);
            scopeVariables.parent = (environment.parent === null) ? null : this.getScopeVariables(environment.parent.parent);
            return scopeVariables;
        };

        /**
         * method to obtain the call stack
         * @return {array} array of functions names with the first element
         *                       containing the current function scope
         *                       the execution is paused in.
         */
        Debug.prototype.getCallStack = function() {
            var runtimeStack = this.__getInfo().runtime_stack;
            var callStack = [];
            for (var i = 0; i < runtimeStack.length; i++) {
                callStack.push(this.__getFunctionName(runtimeStack[i].pc));
            }
            var currentFrame = this.__getFunctionName(this.lastBreakpoint);
            callStack.push(currentFrame);
            return callStack;
        };

        /**
         * method to obtain a zip of callStack and scopeVariables
         * @return {array} array of objects with property 'name' as the function stack name
         *                       and property 'env' as function scope
         */
        Debug.prototype.getDebugInfo = function() {
            var callStack = this.getCallStack();
            var callStackFrames = this.__recurseThroughFrames();
            var debugInfo = [];

            for (i =  0; i < callStack.length; i++) {
                var debugInfoObj = {};
                debugInfoObj.name = callStack[i];
                debugInfoObj.env = this.__filterNativevariables(callStackFrames[i]);
                debugInfo.push(debugInfoObj);
            }
            return debugInfo;
        };

        /**
         * method to obtain the current line number
         * @return {number} current line number
         */
        Debug.prototype.getCurrentLineNumber = function() {
            return this.lineNumbers[this.runtime.pc];
        };

        // Helper Functions (private methods)

        /**
         * helper method to return run time information as returned by the VM
         * @return {object} - an object containing all runtime information including pc,
         *                    runtime_stack and environmnet
         */
        Debug.prototype.__getInfo = function() {
            return this.runtime.get_runtime_information();
        };

        /**
         * helper method to lookup all the variables and functions in the given frame
         * @param  {object} frame - the frame to lookup the variables and 
         *                        functions in
         * @return {object}       an object of variable and function bindings
         */
        Debug.prototype.__getVariablesInFrame = function(frame) {
            var variables = {};
            for (var variable in frame) {
                if (variable === "this") {
                    continue;
                } else if (variable.charAt(0) !== ":" && frame.hasOwnProperty(variable)) {
                    variables[variable] = frame[variable];
                }
            }
            return variables;
        };

        /**
         * helper method to recurisvely iterate through all parent frames and retreive all the 
         *     variable bindings in those frames
         * @param  {array} runtimeStack - an array of all the frames
         * @param  {int} frameNum     the index of the current frame in runtimeStack
         * @return {object}              an object of variable bindings in current and parent frames
         */
        Debug.prototype.__recurseThroughFrames = function() {
            var runtimeStack = this.__getInfo().runtime_stack;
            var environment = this.__getInfo().environment;
            var callStackFrames = [];
            for (var frameNum = 0; frameNum < runtimeStack.length; frameNum++) {
                var frame = runtimeStack[frameNum].environment;
                var variables = this.__getVariablesInFrame(frame);
                callStackFrames.push(variables);
            }
            callStackFrames.push(this.__getVariablesInFrame(environment));
            return callStackFrames;
        };

        /**
         * helper method to retrieve the function in which the given instruction is located
         * @param  {int} instructionNumber - the index of the instruction number 
         *                                     in the instructionArray
         * @return {string}                   the name of the function
         */
        Debug.prototype.__getFunctionName = function(instructionNumber) {
            return this.debugSymbols[instructionNumber].function_name;
        };

        /**
         * helper method to retrieve the index of the first instruction in the given line number
         * @param  {int} linenumber - the line number in the source code (0 based)
         * @return {int}            - the index of the instruction number
         */
        Debug.prototype.__getFirstInstructionNumber = function(linenumber) {
            var instructionNumber = this.lineNumbers.indexOf(linenumber);
            if (instructionNumber !== -1 && this.instructionArray[instructionNumber].name !== "DECLARE") {
                return instructionNumber;
            } else {
                for (var i = 0; i < this.lineNumbers.length; i++) {
                    if (this.instructionArray[i].name === "DECLARE") {
                        continue;
                    }
                    if (linenumber == this.lineNumbers[i]) {
                        return i;
                    }
                }
            }
        };

        /**
         * helper method to remove library functions from the envirnoment form a 
         * stack enviroment
         * @param  {array} stack - the stack env frame build for the debugger UI
         * @return {array}        - the filtered stack env
         */
        Debug.prototype.__filterNativevariables = function(env){
            var new_env = {};
            for(var variable in env){
                if(env.hasOwnProperty(variable)){
                    var element = env[variable];
                    if(element instanceof Closure){
                        if(element.is_native){
                            continue;
                        }
                    }
                    if(element !== undefined && element !== null){                        
                        if(element.__NATIVE_OBJECT__){
                            continue;
                        }
                    }                    
                }

                if(env[variable] === undefined){
                    new_env[variable] = env[variable];
                } else {
                    new_env[variable] = this.runtime.stringify_value(env[variable]);
                }
            }
            return new_env;
        };

        return Debug;

    });