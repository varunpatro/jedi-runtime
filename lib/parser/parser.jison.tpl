
/* description: Parses end executes JediScript expressions. */

%options ranges

/* lexical grammar */
%lex
%x DoubleQuotedString
%x SingleQuotedString
%x QuotedStringEscape

%%

\/\/([^\n\r]*)                                /* skip single-line comments */
\/\*([\u0000-\uffff]*?)\*\/                   /* skip multi-line comments */
\s+                                           /* skip whitespace */

"function"                                    return 'function'
return\s*\n                                   return 'INVALID'
"return"                                      return 'return'
"if"                                          return 'if'
"else"                                        return 'else'
"while"                                       return 'while'
"for"                                         return 'for'
"case"                                        return 'case'
"default"                                     return 'default'
"new"                                         return 'new'
"break"                                       return 'break'
"continue"                                    return 'continue'
"var"                                         return 'var'
"==="                                         return '==='
"="                                           return '='
"{"                                           return '{'
"}"                                           return '}'
";"                                           return ';'
","                                           return ','
"true"                                        return 'true'
"false"                                       return 'false'
"[]"                                          return 'empty_list'
"["                                           return '['
"]"                                           return ']'
"."                                           return '.'

'"'                                           { this.begin('DoubleQuotedString'); this.string = ''; }
"'"                                           { this.begin('SingleQuotedString'); this.string = ''; }
<DoubleQuotedString,SingleQuotedString>\\     this.begin('QuotedStringEscape');
<DoubleQuotedString>'"'                       { yytext = this.string; this.string = undefined; this.popState(); return 'STRING'; }
<SingleQuotedString>"'"                       { yytext = this.string; this.string = undefined; this.popState(); return 'STRING'; }
<QuotedStringEscape>(.|\r\n|\n)               { /* The newlines are there because we can span strings across lines using \ */
    switch (yytext) {
        case '\r\n':
        case '\n':          break;
        case 'b':           this.string += '\b'; break;
        case 'n':           this.string += '\n'; break;
        case 'r':           this.string += '\r'; break;
        case 't':           this.string += '\t'; break;
        case "'":           this.string += "'"; break;
        case '"':           this.string += '"'; break;
        case '\\':          this.string += '\\'; break;
        default:            this.string += '\\' + $1; break;
    }

    this.popState();
}
<DoubleQuotedString>[^"\\]*                   this.string += yytext;
<SingleQuotedString>[^'\\]*                   this.string += yytext;


[A-Za-z_][A-Za-z0-9_]*                        return 'Identifier' /* TODO: non-ASCII identifiers */

[0-9]+("."[0-9]+)?([eE][\-+]?[0-9]+)?\b       return 'FLOAT_NUMBER' /* 3.1, 3.1e-7 */
[0-9]+\b                                      return 'INT_NUMBER'

"+"                                           return '+'
"-"                                           return '-'
"*"                                           return '*'
"/"                                           return '/'
"%"                                           return '%'
"!=="                                         return '!=='
"<="                                          return '<='
">="                                          return '>='
"<"                                           return '<'
">"                                           return '>'
"!"                                           return '!'
"&&"                                          return '&&'
"||"                                          return '||'
"("                                           return '('
")"                                           return ')'
"?"                                           return '?'
":"                                           return ':'

<<EOF>>                                       return 'EOF'
.                                             return 'INVALID'

/lex

/* operator associations and precedence */

%left  ';'
%right '='
%right '?' ':'
%left  '||'
%left  '&&'
%left  '===' '!=='
%left  '<' '>' '<=' '>='
%left  '+' '-'
%left  '*' '/' '%'
%right '!' UMINUS UPLUS
%left  '[' ']'
%left  '.'

%% /* language grammar */

program
    : statements EOF
        { return $1; }
    | statement_block EOF
        { return $1; }
    | empty_block EOF
        { return $1; }
    ;

empty_block
    : '{' '}'
        { $$ = [] }
    ;

statement_block
    : '{' non_empty_statements '}'
        { $$ = $2; }
    ;

statements
    :
        { $$ = []; }
    | non_empty_statements
    ;

non_empty_statements
    : statement non_empty_statements
        {
            if ($1 === Nodes.no_op()) {
                $$ = $2;
            } else {
                $$ = Nodes.pair($1, $2);
            }
        }
    | statement
        {
            if ($1 === Nodes.no_op()) {
                $$ = [];
            } else {
                $$ = Nodes.pair($1, []);
            }
        }
    ;

statement
    : if_statement
{{if week|ormore>8}}
    | while_statement
{{if week|ormore>13}}
    | for_statement
    | break_statement ';'
    | continue_statement ';'
{{/if}}
{{/if}}
    | function_definition
    | return_statement ';'
    | variable_definition ';'
{{if week|ormore>8}}
    | assignment_statement ';'
{{/if}}
    | expression ';'
    | ';'
        { $$ = Nodes.no_op(); }
    ;

if_statement
    : 'if' '(' expression ')' statement_block 'else' statement_block
        { $$ = Nodes.if_statement($3, $5, $7, yylineno); }
    | 'if' '(' expression ')' statement_block else empty_block
        { $$ = Nodes.if_statement($3, $5, $7, yylineno); }
    | 'if' '(' expression ')' empty_block else statement_block
        { $$ = Nodes.if_statement($3, $5, $7, yylineno); }
    | 'if' '(' expression ')' empty_block else empty_block
        { $$ = Nodes.if_statement($3, $5, $7, yylineno); }
    | 'if' '(' expression ')' statement_block 'else' if_statement
        { $$ = Nodes.if_statement($3, $5, Nodes.pair($7, []), yylineno); }
    | 'if' '(' expression ')' empty_block 'else' if_statement
        { $$ = Nodes.if_statement($3, $5, Nodes.pair($7, []), yylineno); }
    ;

{{if week|ormore>8}}
while_statement
    : 'while' '(' expression ')' statement_block
        { $$$ = Nodes.while_statement($3, $5, yylineno) }
    | 'while' '(' expression ')' empty_block
        { $$$ = Nodes.while_statement($3, $5, yylineno) }
    ;
{{/if}}

{{if week|ormore>13}}
for_statement
    : 'for' '(' for_initialiser ';' expression ';' for_finaliser ')' statement_block
        { $$$ = Nodes.for_statement($3, $5, $7, $9, yylineno); }
    | 'for' '(' for_initialiser ';' expression ';' for_finaliser ')' empty_block
        { $$$ = Nodes.for_statement($3, $5, $7, $9, yylineno); }
    ;

for_initialiser
    : expression
    | variable_definition
    | assignment_statement
    |
    ;

for_finaliser
    : assignment_statement
    | expression
    |
    ;

break_statement
    : 'break'
        { $$$ = Nodes.break_statement(yylineno); }
    ;

continue_statement
    : 'continue'
        { $$$ = Nodes.continue_statement(yylineno); }
    ;
{{/if}}

function_definition
    : 'function' identifier '(' identifiers ')' statement_block
        { $$ = Nodes.variable_definition($2, Nodes.function_definition($2, $4, $6, @1, @6), yylineno); }
    | 'function' identifier '(' identifiers ')' empty_block
        { $$ = Nodes.variable_definition($2, Nodes.function_definition($2, $4, $6, @1, @6), yylineno); }
    ;

return_statement
    : 'return' expression
        { $$ = Nodes.return_statement($2, yylineno); }
    ;

variable_definition
    : 'var' identifier '=' expression
        { $$ = Nodes.variable_definition($2, $4, yylineno); }
    ;

{{if week|ormore>8}}
assignment_statement
    : expression '=' expression
        {
            if ($1.tag === 'variable') {
                $$$ = Nodes.assignment($1, $3, yylineno);
{{if week|ormore>10}}
            } else if ($1.tag === 'property_access') {
                $$$$$$ = Nodes.property_assignment($1.object, $1.property, $3, yylineno);
{{/if}}
            } else {
                error('parse error in line ' + yylineno + ": " + yytext);
            }
        }
    ;
{{/if}}

expression
    :
    expression '+' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | expression '-' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | expression '*' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | expression '/' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | expression '%' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | '-' expression %prec UMINUS
        { $$ = Nodes.eager_binary_expression(0, $1, $2, yylineno); }
    | '+' expression %prec UPLUS
        { $$ = Nodes.eager_binary_expression(0, $1, $2, yylineno); }
    | '!' expression
        { $$ = Nodes.eager_unary_expression($1, $2, yylineno); }
    | expression '&&' expression
        { $$ = Nodes.boolean_operation($1, $2, $3, yylineno); }
    | expression '||' expression
        { $$ = Nodes.boolean_operation($1, $2, $3, yylineno); }
    | expression '===' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | expression '!==' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | expression '>' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | expression '<' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | expression '>=' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
    | expression '<=' expression
        { $$ = Nodes.eager_binary_expression($1, $2, $3, yylineno); }
{{if week|ormore>10}}
    | expression '[' expression ']'
        { $$$ = Nodes.property_access($1, $3, yylineno); }
{{/if}}
{{if week|ormore>3}}
    /* Because we need to use the Math library. */
    | expression '.' identifier
        { $$$ = Nodes.property_access($1, $3, yylineno); }
{{/if}}
    | '(' expression ')'
        { $$ = $2; }
    | constants
    | identifier
        { $$ = Nodes.variable($1, yylineno); }
    | '(' expression ')' '(' expressions ')'
        { $$ = Nodes.application($2, $5, yylineno); }
{{if week|ormore>10}}
    | array_literal
    | object_literal
{{/if}}
    | identifier '(' expressions ')'
        { $$ = Nodes.application(Nodes.variable($1, yylineno), $3, yylineno); }
{{if week|ormore>3}}
    | expression '.' identifier '(' expressions ')'
        { $$$ = Nodes.object_method_application($1, $3, $5, yylineno); }
{{/if}}
{{if week|ormore>10}}
    | new identifier '(' expressions ')'
        { $$$ = Nodes.construction($2, $4, yylineno); }
{{/if}}
    | function_expression
    | expression '?' expression ':' expression
        { $$ = Nodes.ternary($1, $3, $5, yylineno); }
    ;

constants
    : 'STRING'
        { $$ = yytext; }
    | 'FLOAT_NUMBER'
        { $$ = parseFloat(yytext); }
    | 'INT_NUMBER'
        { $$ = parseInt(yytext, 10); }
    | 'true'
        { $$ = true; }
    | 'false'
        { $$ = false; }
    | 'empty_list'
        { $$ = Nodes.empty_list(yylineno); }
    ;

{{if week|ormore>10}}
array_literal
    : '[' expressions ']'
        { $$$ = Nodes.array_literal($2, yylineno); }
    ;

object_literal
    : '{' non_empty_object_literal_statements '}'
        { $$$ = Nodes.object_literal($2, yylineno); }
    | empty_block
        { $$$ = Nodes.object_literal([], yylineno); }
    ;

non_empty_object_literal_statements
    : object_literal_statement ',' non_empty_object_literal_statements
        { $$$ = Nodes.pair($1, $3); }
    | object_literal_statement
        { $$$ = Nodes.pair($1, []); }
    ;

object_literal_statement
    : identifier ':' expression
        { $$$ = Nodes.pair($1, $3); }
    ;
{{/if}}

function_expression
    : 'function' '(' identifiers ')' statement_block
        { $$ = Nodes.function_definition('lambda', $3, $5, @1, @5); }
    | 'function' '(' identifiers ')' empty_block
        { $$ = Nodes.function_definition('lambda', $3, $5, @1, @5); }
    ;

expressions
    : non_empty_expressions
        { $$ = $1; }
    | /* NOTHING */
        { $$ = []; }
    ;

non_empty_expressions
    : expression ',' non_empty_expressions
        { $$ = Nodes.pair($1, $3); }
    | expression
        { $$ = Nodes.pair($1, []); }
    ;

identifiers
    : non_empty_identifiers
        { $$ = $1; }
    | /* NOTHING */
        { $$ = []; }
    ;

non_empty_identifiers
    : identifier ',' non_empty_identifiers
        { $$ = Nodes.pair($1, $3); }
    | identifier
        { $$ = Nodes.pair($1, []); }
    ;

identifier
    : 'Identifier'
        { $$ = yytext; }
    ;

%%
{{includes}}
