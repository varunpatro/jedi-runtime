# High Performance Virtual Machine for JediScript.

Current maintainer: Evan Sebastian

# Introduction
This project comprises Parser, Compiler, and a Virtual Machine for JediScript.
It makes no assumption of the underlying platform, meaning that except for one
source file for configuration, the library should be portable.

This project = One platform specific "lib/jfdi.js" file + other files under lib

# General Usage
Simply include the concatenated javascript file generated every build in the HTML.
Requirejs is already included and need not be imported separately.

    var source = "1 + 2;";
    var Parser = require('lib/parser/parser');
    var compile = require('lib/compiler/compiler');
    var Runtime = require('lib/vm/runtime');

    var ast = Parser.parse(source);
    var instructions = compile(ast).instructions;
    var result = new Runtime().execute_instruction(instructions);

# Project Structure

    lib       - Source folder, everything inside here will be concatenated together with
            jfdi.js as platform dependent entry point
    test      - Test folder
    coverage  - Test coverage report
    doc       - Documentation of instruction semantics.

# Prerequisites.
This project is completely client side with each file defined as a Require.js module.
However, we will be using many convenient tools (linters, test runners) from the Node.js stack.

1. Mercurial.
2. Node.js (~0.10).Installation instruction can be found in Node.js website
3. Grunt-cli (~0.4.5).
    Grunt is used to run the tasks (linting, concatenating, testing)
    Once you install Node, you should be able to use the `npm` command. You can then run:

        npm install -g grunt-cli

4. Google Chrome (~39). The unit tests will be run in Google Chrome.

# Getting Started
1. Clone this repository.
2. Run `npm install` to install the dependencies.
3. Run `grunt` to build the javascript file according to Gruntfile.js
4. Run `grunt test` to execute a new Chrome window running all the test cases.

# Code Coverage
Code coverage for every file under `lib` is generated automatically each
invocation of `grunt test` and can be seen in the file
`coverage/<Chrome version>/index.html`

Try to make at least 95% of the code covered.

# How to Debug tests using Karma
Instead of running `grunt test`, you can run `./node_modules/karma/bin/start` to start
a Karma instance in your default browser. You can then proceed to run/debug the test cases
by clicking the button "DEBUG" in the Karma interface.

Please contact the maintainer if you have any questions or to file a bug report.