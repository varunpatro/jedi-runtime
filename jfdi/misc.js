define(function() {
return {
    initialize: function(week, export_symbol) {
				if (typeof window !== 'undefined') {
		    	export_symbol('alert', alert);
				}
				if (week === 4) {
		        export_symbol('display', display, "(message)");
				}
				if (week >= 5) {
		        export_symbol('is_null', is_null, "(x)");
		        export_symbol('is_number', is_number, "(x)");
		        export_symbol('is_string', is_string, "(x)");
		        export_symbol('is_boolean', is_boolean, "(x)");
		        export_symbol('is_object', is_object, "(x)");
		        export_symbol('is_function', is_function, "(x)");
		        export_symbol('is_NaN', is_NaN, "(x)");
		        export_symbol('is_undefined', is_undefined, "(x)");
		        export_symbol('has_own_property', has_own_property);
		        export_symbol('is_array', is_array, "(x)");
		        export_symbol('runtime', runtime);
		        export_symbol('error', error, "(message)");
		        export_symbol('newline', newline);
		        export_symbol('random', random);
		        export_symbol('timed', timed, "(f)");
		        export_symbol('read', read);
		        export_symbol('write', write);
			  }
				if (week >= 12) {
		        	export_symbol('apply_in_underlying_javascript',
						apply_in_underlying_javascript);
				}
    }
};
});
