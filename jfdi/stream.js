define(function () {

return {
  initialize: function(week, export_symbol) {
      if (week >= 11) {
          export_symbol('stream_tail', stream_tail, "(xs)");
          export_symbol('is_stream', is_stream, "(xs)");
          export_symbol('list_to_stream', list_to_stream, "(xs)");
          export_symbol('stream_to_list', stream_to_list, "(xs)");
          export_symbol('stream', stream);
          export_symbol('stream_length', stream_length, "(xs)");
          export_symbol('stream_map', stream_map, "(f, xs)");
          export_symbol('build_stream', build_stream, "(n, f)");
          export_symbol('stream_for_each', stream_for_each, "(f, xs)");
          export_symbol('stream_reverse', stream_reverse, "(xs)");
          export_symbol('stream_to_vector', stream_to_vector, "(xs)");
          export_symbol('stream_append', stream_append, "(xs, ys)");
          export_symbol('stream_member', stream_member, "(v, xs)");
          export_symbol('stream_remove', stream_remove, "(v, xs)");
          export_symbol('stream_remove_all', stream_remove_all, "(v, xs)");
          export_symbol('stream_filter', stream_filter, "(pred, xs)");
          export_symbol('enum_stream', enum_stream, "(start, end)");
          export_symbol('integers_from', integers_from, "(n)");
          export_symbol('eval_stream', eval_stream, "(xs, n)");
          export_symbol('stream_ref', stream_ref, "(xs, n)");
      }
  }
};
});
