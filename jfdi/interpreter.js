define(function() {
return {
	 initialize : function(week, export_symbol) {
		 if (week >= 10) {
     		parser_register_native_function('Object', Object);
	 	 }
		 if (week >= 12) {
			 export_symbol('parse', parse, "(source_text)");
			 export_symbol('apply_in_underlying_javascript',
				 apply_in_underlying_javascript);
			 export_symbol('parse_and_evaluate', parse_and_evaluate);
			 export_symbol('parser_register_native_function',
			 	parser_register_native_function);
			 export_symbol('parser_register_native_variable',
			 	parser_register_native_variable);
			 export_symbol('is_object', is_object);
			 export_symbol('JSON', JSON);
			 export_symbol('Function', Function);
			 export_symbol('RegExp', RegExp);
		 }
	 }
};

});
