/*
 * This main file is configured for CS1101S JFDI Academy.
 * It should be loaded on JFDI Academy after the bundled jedi-runtime file.
 */
define(function(require) {
    var API = {}
    var vm;
    var vm_debug;
    var lines;
    var debug;
    var orig_offset;
    var symbols = [];
    var parser_week;
    var Parser;
    var JediRuntime = require("../dist/jedi-runtime");
    var Compiler = JediRuntime.Compiler;
    var Runtime  = JediRuntime.Runtime;
    var Debug = JediRuntime.Debug;

    API.version = JediRuntime.version;

    function get_current_context() {
        if (typeof window !== 'undefined') {
          return window;
        } else {
          return global;
        }
    }

    function handle_parser_error(exception) {
        throw exception.message;
    }

    function handle_compiler_error(exception, source) {
        lines = source.split(/\r?\n/);
        message = "\nOn line " + (e.line + 1) + "\n\n";
        message += lines[e.line] + "\n\n";
        message += e.message;
        throw message;
    }

    API.initialize = function(week, context) {
        // Handle Old API Call
        if (typeof week === 'object' ) {
          var temp = context;
          context = week;
          week = context;
        }
        var LibraryLoader = require('library-loader');
        context = context || get_current_context();
        week = week || 13;
        Parser = JediRuntime.Parser(week);
        context.export_symbol = function(name, fun, unused_2) {
            context[name] = fun;
            symbols.push(name);
        };
        // Required by interpreter library
        LibraryLoader.initialize(week, context.export_symbol);
    };

    API.parse = function(src) {
        var ast;
        try {
            ast = Parser.parse(src);
        } catch (e) {
            handle_parser_error(e);
        }

        return ast;
    };

    API.get_javascript = function(name) {
        return API.to_javascript(vm.environment.get(name));
    };

    API.parse_and_evaluate = function(src, reinitialize, context) {
        var instructions, ast, artifact, result, message, offset,
          root_environment;
        context = context || get_current_context();
        reinitialize = reinitialize || false;
        ast = this.parse(src);

        try {
            artifact = Compiler.compile(ast, src);
        } catch (e) {
            handle_compiler_error(e);
        }

        try {
            instructions = artifact.instructions;
            if (reinitialize || vm === undefined) {
                debug = artifact.debug;
                vm = new Runtime(symbols, context);
                lines = src.split(/\r?\n/);
                orig_offset = instructions.length;
                offset = instructions.length;
                root_environment = vm.environment
                result = vm.execute_instruction(instructions).value;
            } else {
                offset = vm.instruction_array.length;
                root_environment = vm.environment
                result = vm.execute_more_instruction(instructions).value;
            }
            return result;
        } catch (e) {
            var message = "";
            if (vm.pc < orig_offset && debug[vm.pc] !== undefined
                  && debug[vm.pc].line !== undefined) {
              message = "\nOn line " + (debug[vm.pc].line + 1) + "\n\n";
              message += lines[debug[vm.pc].line] + "\n\n";
            } else if (artifact.debug[vm.pc - offset] !== undefined &&
                       artifact.debug[vm.pc - offset].line !== undefined) {
              lines = src.split(/\r?\n/);
              message = "\nOn line " +
                (artifact.debug[vm.pc - offset].line + 1) + "\n\n";
              message += lines[artifact.debug[vm.pc - offset].line] + "\n\n";
            }
            vm.environment = root_environment;
            if (e.message !== undefined) {
                throw message + e.message + (JSON.stringify(e.line) || "");
            } else {
                throw message + e;
            }
        }
    };

    API.debug = function(src, reinitialize, breakpointsArray, context) {
        var instructions, ast, artifact, result, message, offset,
          root_environment;
        context = context || get_current_context();
        reinitialize = reinitialize || false;

        try {
            ast = Parser.parse(src);
        } catch (e) {
            handle_parser_error(e);
        }

        try {
            artifact = Compiler.compile(ast, src);
        } catch (e) {
            handle_compiler_error(e);
        }

            debug = artifact.debug;
            instructions = artifact.instructions;

        try {
            if(reinitialize || vm_debug === undefined) {
                lines = src.split(/\r?\n/);
                orig_offset = instructions.length;
                vm_debug = new Debug(src, symbols, context);
            }
            vm_debug.setBreakpoints(breakpointsArray);

            offset = instructions.length;
            root_environment = vm_debug.runtime.environment
            var result = vm_debug.start();

            return {
                stack: vm_debug.getDebugInfo(),
                status: result.status,
                line_no: vm_debug.getCurrentLineNumber()
            };
        } catch (e) {
            var message = "";
            if (vm_debug.runtime.pc < orig_offset && debug[vm_debug.runtime.pc] !== undefined
                  && debug[vm_debug.runtime.pc].line !== undefined) {
              message = "\nOn line " + (debug[vm_debug.runtime.pc].line + 1) + "\n\n";
              message += lines[debug[vm_debug.runtime.pc].line] + "\n\n";
            } else if (artifact.debug[vm_debug.runtime.pc - offset] !== undefined &&
                       artifact.debug[vm_debug.runtime.pc - offset].line !== undefined) {
              lines = src.split(/\r?\n/);
              message = "\nOn line " +
                (artifact.debug[vm_debug.runtime.pc - offset].line + 1) + "\n\n";
              message += lines[artifact.debug[vm_debug.runtime.pc - offset].line] + "\n\n";
            }
            vm_debug.runtime.environment = root_environment;
            if (e.message !== undefined) {
                throw message + e.message + (JSON.stringify(e.line) || "");
            } else {
                throw message + e;
            }
        }
    };


    API.stringify = function(result) {
        if (result === undefined) {
            return "undefined";
        } else {
            return vm.stringify_value(result);
        }
    };
    API.to_javascript = function(result) {
        if (result === undefined) {
            return undefined;
        } else {
            return vm.vm_value_to_javascript(result);
        }
    };
    return API;
});
