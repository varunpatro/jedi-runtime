define(function() {
return {
initialize : function(week, export_symbol) {
    if (week >= 5) {
        export_symbol('array_test', array_test);
        export_symbol('pair', pair, "(x, y)");
        export_symbol('is_pair', is_pair, "(x)");
        export_symbol('head', head, "(xs)");
        export_symbol('tail', tail, "(xs)");
        export_symbol('is_empty_list', is_empty_list, "(xs)");
        export_symbol('is_list', is_list, "(x)");
        export_symbol('display', display);
        export_symbol('list', list);
        export_symbol('list_to_vector', list_to_vector, "(xs)");
        export_symbol('vector_to_list', vector_to_list, "(v)");
        export_symbol('length', length, "(xs)");
        export_symbol('map', map, "(f, xs)");
        export_symbol('build_list', build_list, "(n, f)");
        export_symbol('for_each', for_each, "(f, xs)");
        export_symbol('list_to_string', list_to_string, "(xs)");
        export_symbol('reverse', reverse, "(xs)");
        export_symbol('append', append, "(xs, ys)");
        export_symbol('member', member, "(v, xs)");
        export_symbol('remove', remove, "(v, xs)");
        export_symbol('remove_all', remove_all, "(v, xs)");
        export_symbol('equal', equal, "(x, y)");
        if (week >= 8) {
            export_symbol('assoc', assoc, "(v, xs)");
            export_symbol('set_head', set_head, "(xs, x)");
            export_symbol('set_tail', set_tail, "(xs, x)");
        }
        export_symbol('filter', filter, "(pred, xs)");
        export_symbol('enum_list', enum_list, "(start, end)");
        export_symbol('list_ref', list_ref, "(xs, n)");
        export_symbol('accumulate', accumulate, "(op, initial, xs)");
    }
}
}
});
