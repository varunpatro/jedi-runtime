
define(function(require) {
    var initialize = function(week, export_symbol) {
        export_symbol("Infinity", Infinity);
        export_symbol("NaN", NaN);
        export_symbol("undefined", undefined);
        export_symbol("Math", Math);
        if (week >= 6) {
            export_symbol("String", String);
        }
        var Misc = require('misc');
        Misc.initialize(week, export_symbol);

        if (week >= 5) {
          var List = require('list');
          export_symbol("parseInt", parseInt);
          export_symbol("prompt", prompt);
          List.initialize(week, export_symbol);
        }

    	if (week >= 10) {
          var Object_ = require('object');
          Object_.initialize(week, export_symbol);
        }

        if (week >= 11) {
            var Stream = require('stream');
            Stream.initialize(week, export_symbol);
        }

        if (week >= 12) {
            var Interpreter = require('interpreter');
            Interpreter.initialize(week, export_symbol);
        }
    };
    return { initialize: initialize };
});
