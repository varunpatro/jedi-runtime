#Compiler Specification

This compiler depends on the original interpreter parser. Its input is a parse tree generated from the source and output is the virtual machine instructions that executes the programme.

##Usage and output
Input: the output of parser on a given source programme.

  See the parser library for its usage

Output: an object

  Field instructions

    An array of Instruction objects (see vm library)

  Field debug

    An array of object registering the line number of each corresponding Instruction in `instruction_array` and the name of the function this instruction belongs to

    Each object has

      Field `function_name`
        The original name of the function at the place of definition

      Field `line`
        The line number in the source this instruction corresponds to

##Language Semantics
This section describes how the compiler translate parse tree elements to VM instructions

###Variables

####Variable declarations

Example

    var var_name = expression;

Semantics

1. Write instructions to compute `expression`
2. Write an instruction

    STORE var_name

Note

All locally declared variables in each function scope are scanned and hoisted.

####Variable lookup

Example

    var_name;

Semantics

1. Write an instruction

    LDS var_name

###Controls

####If ... Else

Example

    if (bool_expression) {
    	consequent;
    } else {
    	alternative;
    }

Semantics

1. Write instructions to compute `bool_expression`
2. Write a placeholder, named `predicate_fail_jump`
3. Write instructions to compute `consequent`
4. Write a placeholder, named `consequent_exit_jump`
5. Replace the placeholder `predicate_fail_jump` with an Instruction

    JOFR (next_instruction_address - predicate_fail_jump_address)

6. Write instructions to compute `alternative`
7. Replace the placeholder `consequent_exit_jump` with an Instruction

    JOFR (next_instruction_address - consequent_exit_jump_address)

####Ternary expression

Example

    bool_expression ? ternary_consequent_expression : ternary_alternative_expression

Semantics

1. Write instructions to compute `bool_expression`
2. Write a placeholder, named `predicate_fail_jump`
3. Write instructions to compute `ternary_consequent_expression`
4. Write a placeholder, named `consequent_exit_jump`
5. Replace the placeholder `predicate_fail_jump` with an Instruction

    JOFR (next_instruction_address - predicate_fail_jump_address)

6. Write instructions to compute `ternary_alternative_expression`
7. Replace the placeholder `consequent_exit_jump` with an Instruction

    JOFR (next_instruction_address - consequent_exit_jump_address)

####While statement

Example

    while (predicate_boolean_expression) {
    	while_statements;
    }

Semantics

1. Record the starting address of instructions computing `predicate_boolean_expression`, named `predicate_address`
2. Write instructions to compute `predicate_boolean_expression`
3. Write a placeholder, named `predicate_fail_jump`
4. Write instructions to compute `while_statements` in a new loop control scope, discarding all intermediate results
5. Write an instruction

    GOTOR (predicate_address - current_instruction_address)

6. Replace the placeholder `predicate_fail_jump` with an Instruction

    JOFR (next_instruction_address - predicate_fail_jump_address)

7. Replace all break and continue placeholders in this loop control scope with proper jump configurations

####For statement

Example

    for (initialiser_statement; predicate_boolean_expression; finaliser_expression) {
    	for_statements;
    }

Semantics

1. Write instructions to compute `initialiser_statement`
2. Record the starting address of instructions computing `predicate_boolean_expression`, named `predicate_address`
3. Write instructions to compute `predicate_boolean_expression`
4. Write a placeholder, named `predicate_fail_jump'
5. Write instructions to compute `for_statements` in a new loop control scope, discarding all intermediate results
6. Record the starting address of instructions computing `finaliser_expression`, named `finaliser_address`
7. Write instructions to compute `finaliser_expression`
8. Write an Instruction

    JOFR (current_instruction_address - predicate_address)

9. Replace the placeholder `predicate_fail_jump` with an Instruction

    JOFR (next_instruction_address - predicate_fail_jump_address)

10. Replace all break and continue placeholders in this loop control scope with proper jump configurations and `finaliser_address` is used for determine jumps for continue statements

####Break statement

Example

    break;

Semantics

1. Check if the statement resides in a loop control scope. If the scope is undefined, throws an exception
2. Write a placeholder and register its address in the loop control scope so that it will jump out of loop

####Continue statement

Example

    continue;

Semantics

1. Check if the statement resides in a loop control scope. If the scope is undefined, throws an exception
2. Write a placeholder and register its address in the loop control scope so that it will jump to `finaliser_address` for `for` statements, or `predicate_address` for `while` statements

###Function and Applications

####Function declaration

Example

    function function_name(function_parameters) {
    	function_body_statements;
    }

Semantics

1. Create a new function scope, and include `this` as parameter
2. Check `function_parameters` for duplicate parameter names
3. Write instructions computing `function_body_statements` into the instruction array of the new function scope
4. Write a placeholder for future substitution into actual function loading instruction
5. Write instructions to create an object and assign it to the `prototype` property of the function
6. Record `function_name` as a declared variable
7. Write an Instruction

    STORE function_name

####Function value definition

Example

    (function(function_parameters){
    	function_body_statements;
    })

Semantics

1. Create a new function scope, and include `this` as parameter
2. Check `function_parameters` for duplicate parameter names
3. Write instructions computing `function_body_statements` into the instruction array of the new function scope
4. Write a placeholder for future substitution into actual function loading instruction
5. Write instructions to create an object and assign it to the `prototype` property of the function

####Function Application

Example

    function_variable(arguments);

Semantics

1. Write instructions to evaluate `function_variable`
2. Write an Instruction

    LDU

3. Write instructions to iteratively evaluate each elements in `arguments`
4. Write an Instruction

    CALL (length(arguments) + 1)

####Primitive Operators

Example

    expression1 operator expression2

Semantics

1. Check if `operator` is primitive; otherwise failure
2. Write instructions computing expression1
3. Write instructions computing expression2
4. Write an Instruction corresponding to the operator

####Function Return

Example

    return expression;

Semantics

1. Check if `expression` is tail-optimisable; if yes, process with tail optimisation
2. Write instructions computing `expression`
3. Write an Instruction

    RTN

####Function Tail Optimization

Example

    return function_variable(function_parameters);

Semantics

1. Check if the return expression is a function application, or construction expression, or object method invocation; if not, process without tail optimisation
2. Write instructions computing the function call `function_variable(function_parameters)`
3. Write an Instruction

    TAILCALL

###Record

####Object Expression

Example

    {
    	key: value,
    	...
    }

Semantics

1. For each key-value pair, write instructions computing value, then key
2. Record the number of key-value pairs as `count`
3. Write an Instruction

    LDCO (count)

####Object Property Access

Example

    object[property];

Semantics

1. Write instructions computing `object`
2. Write instructions computing `property`
3. Write an Instruction

    READPS

####Object Property Assignment

Example

    object[property] = expression;

Semantics

1. Write instructions computing `object`
2. Write instructions computing `property`
3. Write instructions computing `expression`
4. Write an Instruction

    STOREPS

####Construction Expression

Example

    new MyObject(arguments);

Semantics

1. Write instructions computing `MyObject`
2. Claim one temporary variable `temp_var`
3. Write Instructions

    STORE (temp_var)
    LDS (temp_var)
    LDS (temp_var)
    LDCS prototype
    READPS
    LDCS __proto__
    LDCO 1

4. Relinquish `temp_var`
5. Write instructions computing each element in `arguments`
6. Write an Instruction

    CALL (length(arguments) + 1)

####Object Method Invocation

Example

    object[method](arguments);

Semantics

1. Write instructions computing `object`
2. Claim one temporary variable `temp_var`
3. Write Instructions

    STORE (temp_var)
    LDS (temp_var)

4. Write instructions computing `method`
5. Write Instructions

    READPS
    LDS (temp_var)

6. Write instructions computing each element in `arguments`
7. Write an Instruction

    CALL (length(arguments) + 1)

###Array

####Array Expression

Example

    [vals, ...];

Semantics

1. Write instructions computing each element in `vals`
2. Record the number of elements in `vals` as `count`
3. Write an Instruction

    LDCA (count)