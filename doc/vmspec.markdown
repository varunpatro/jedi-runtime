#Virtual Machine Specification for JediScript (v1)

Up to JediScript Week 3.

## Components

1. **Instructions Library**
    Contains the definition of all instructions to be used by the compiler.

2. **Runtime Environment**

    One big loop that executes instructions array until `DONE` is reached.

    It has a state which comprises :
    1. Operand Stack.
       Store temporary operands used in many instructions.
    2. Runtime Stack.
       Store stack frames for function call.
    3. Environment.
       Store mapping between identifier and values.

3. **Foreign Function Interface (FFI)**

    Responsible to handle function call to native javascript function.
    This component call and convert the return value back to JediScript types.


(*) To be discussed later

##Instructions

### Constants

#### LoaD Undefined (LDU)

Syntax:

    LDU

Semantics:

1. Push `undefined` to operand stack
2. Increment the program counter.

#### LoaD Constant Number (LDCN)

Syntax:

    LDCN n

Where n is a valid JediScript number.

Semantics :

1. Push n to operand stack
2. Increment the program counter.

#### LoaD Constant String (LDCS)

Syntax:

    LDCS s

Where s is a valid JediScript string.

Semantics :

1. Push s to operand stack
2. Increment the program counter.

#### LoaD Constant Boolean (LDCB)

Syntax:

    LDCB [true|false]

Semantics :

1. Push [true|false] to operand stack
2. Increment the program counter.

### Operators

#### Plus (PLUS)

Syntax:

    PLUS

Semantics :

1. Pop 2 values from the operand stack
2. Do a type-safe `+` of the values
3. Push the result back to the operand stack
4. Increment the program counter.

#### Subtract (SUB)

Syntax:

    SUB

Semantics :

1. Pop 2 values from the operand stack
2. Do a type-safe `-` of the values
3. Push the result back to the operand stack
4. Increment the program counter.

#### Times (TIMES)

Syntax:

    TIMES

Semantics :

1. Pop 2 values from the operand stack
2. Do a type-safe `*` of the values
3. Push the result back to the operand stack
4. Increment the program counter.

#### Divide (DIV)

Syntax:

    DIV

Semantics :

1. Pop 2 values from the operand stack
2. Do a type-safe `/` of the values
3. Push the result back to the operand stack
4. Increment the program counter.

Note : The return value for common arithmetic error such as
division by zero will follow Javascript.

#### Modulo (MOD)

Syntax:

    MOD

Semantics :

1. Pop 2 values from the operand stack
2. Do a type-safe `%` of the values
3. Push the result back to the operand stack
4. Increment the program counter.

#### Equal (EQUAL)

Syntax:

    EQUAL

Semantics :

1. Pop 2 values from the operand stack
2. Do a type-safe `===` of the values
3. Push the result back to the operand stack
4. Increment the program counter.

#### Not Equal (NOTEQUAL)

Syntax:

    NOTEQUAL

Semantics :

1. Pop 2 values from the operand stack
2. Do a type-safe `!==` of the values
3. Push the result back to the operand stack
4. Increment the program counter.

#### Boolean Negation (BNEG)

Syntax:

    BNEG

Semantics :

1. Pop 1 value from the operand stack
2. Do a type-safe `!` of the value
3. Push the result back to the operand stack
4. Increment the program counter.

#### Arithmetic Negation (ANEG)

Syntax:

    ANEG

Semantics :

1. Pop 1 value from the operand stack
2. Do a type-safe `-` of the value
3. Push the result back to the operand stack
4. Increment the program counter.

#### Less Than (LESSTHAN)

Syntax:

    LESSTHAN

Semantics :

1. Pop 2 values from the operand stack
2. Do a type safe `<` of the values
3. Push the result back to the operand stack
4. Increment the program counter.


#### Less Than or Equal (LESSTHANEQ)

Syntax:

    LESSTHANEQ

Semantics :

1. Pop 2 values from the operand stack
2. Do a type safe `<=` of the values
3. Push the result back to the operand stack
4. Increment the program counter.


#### Greater Than (GREATERTHAN)

Syntax:

    GREATERTHAN

Semantics :

1. Pop 2 values from the operand stack
2. Do a type safe `>` of the values
3. Push the result back to the operand stack
4. Increment the program counter.


#### Greater Than or Equal (GREATERTHANEQ)

Syntax:

    GREATERTHANEQ

Semantics :

1. Pop 2 values from the operand stack
2. Do a type safe `>=` of the values
3. Push the result back to the operand stack
4. Increment the program counter.

### Conditional

### Jump on False Relative (JOFR)
Syntax:

    JOFR i

Where i is the number of instruction.

Semantics:

1. Pop 1 value from the operand stack.
2. If the value is false, goto instruction number pc + i.

#### Go to Instruction Relative (GOTOR)

Syntax:

    GOTOR i

Where i is the number of instruction.

Semantics:

1. Change the program counter to pc + i.

### Identifier

#### LoaD Symbol (LDS)

Syntax:

    LDS x

Where x is a valid JediScript identifier

Semantics:

1. If `environment[x]` is not defined, throw a `UndefinedVariable` exception.
2. Else, push `environment[x]` to the operand stack.

#### Declare (DECLARE)

Syntax:

    DECLARE x

Where x is a valid JediScript identifier

Semantics:

1. Add a binding of "x" in the current of x with the value undefined.

Remarks:
This assumes that every JediScript value will consume only 1 space in operand stack.
So far, this is true because of the eager evaluation and because `LDF` is used in place of `LDFS`

#### Store (STORE)

Syntax:

    STORE x

Where x is a valid JediScript identifier

Semantics:

1. Pop the operand stack and assign to `environment[x]`, possibly replacing the previous value.

### Function

#### LoaD Function (LDF)

Syntax:

    LDF [n, fp1, fp2, fp3..]

Where n is the address of the first instruction of the function body.
fp1, fp2, etc are the name of the formal parameter.

Semantics:

1. Push a new Closure(n,[fp1,fp2,..],env) to the operandStack.
2. Increment the program counter

#### Call (CALL)
Syntax:

    CALL n

Where n is the length of the formal parameters.

Semantics:

1. Examine the closure created (i.e `operand_stack[n + 1]`)
2. If `environment[closure.address]` is a native JavaScript function
    * Execute the external function using the arguments.
    * Accept and push the result to the operand stack.
3. Else if `environment[closure.address]` is valid, it is a call to a user defined function.
    * Push a new StackFrame(operand stack, pc, environment) to the runtime stack.
    * Create new operand stack.
    * Change program counter to the actual address of the function.
    * Extend environment with the function's arguments.
4. Else it throw an `UndefinedVariable` exception.

#### Tail Call (TAILCALL)
Syntax:

    TAILCALL n

Semantics:
Similar to CALL, but this does not push anything to runtime stack.

#### Construct (CONSTRUCT)

Syntax:

    CONSTRUCT n

Where n is the length of the formal parameters.

Semantics:
Similar to CALL, except:
* this does not check if it is a native JavaScript function.
* The StackFrame created has a flag to indicate that it is an object construction call

#### ReTurN (RTN)

Syntax:

    RTN

Semantics:

1. Pop the last state from the runtime stack.
2. Restore topmost StackFrame to current state.
3. If it returns from an object construction call and the return value is undefined
    * set the return value to 'this'
4. Else set the return value to the value returned by the function
5. Push the return value to the operand stack.

#### Pop (POP)

Syntax:

    POP

Semantics:

1. Pop the operandStack.
2. Increment the program counter.

### Records

#### LoaD Constant Object (LDCO)

Syntax:

    LDCO n

Where n is the number of properties
Semantics:

1. Create an empty object
2. For n iterations
    * Pop 2 values from the operand stack
    * Add a property to the created object using the first popped value as the key and the second popped value as the value of the key-value pair
3. Push the object into the operand stack
4. Increment the program counter.

#### REad Property Symbolic (READPS)

Syntax:

    READPS

Semantics:

1. Pop 2 values from the operand stack. First is the value, second is the object.
2. If the object (the second value popped) has the property specified by the first value popped, push that value to the operand stack.
3. Else, push undefined to the operand stack.

#### STORE Property Symbolic (STOREPS)

Syntax:

    STOREPS

Semantics:

1. Pop 3 values from the operand stack. First is the value, second is the property, third is the object.
2. Assign the value as the object's property i.e object[property] = value

### Arrays

#### LoaD Constant Array (LDCA)

Syntax:

    LDCA n

Where n is the number of elements

Semantics:

1. Create an empty array.
2. For n iterations, pop a value from the operand stack and push it to the array created.
3. Reverse the array.
4. Push the array into the operand stack.
5. Increment the program counter.