# JediScript Debugger User Stories (v1)

## Setting Breakpoints

1. As a student, I want to be able to create breakpoints so that I can find out what’s wrong with my code
2. As a student, I want to be able to resume from a breakpoint one line at a time to figure out which line of code is causing trouble.
3. As a student, I want to be able to resume from a breakpoint one line at a time, but skipping a function call, so that I can ignore a faulty function and see if the rest of my program works.
4. As a student, I want to be able to resume from a breakpoint by exiting the current function I’m in and manually giving it a return value, so that I can ignore a faulty function and carry on with the rest of my program.

## Environment Model Visualization

5. As a student, I want to be able to see all attributes and methods of an object, to be able to identify it (instead of seeing `[object Object]`).
6. As a student, I want to be able to see the inheritance ancestry of an object, to be able to understand OOP betterAs a student, I want to be able to see all variable bindings in my current scope, as well as variable bindings in ‘higher’ scopes, so that I can understand the environment model better.
7. As a student, I want to be able to see all the arguments fed into a particular function for every instance it is called, to try to identify an error with the code.
8. As a student, I want to be able to update variables when my program stops at a breakpoint, to help debug the code.
9. As a student, I want to be able to count the number of times I call a particular function, so that I analysing the runtime is easier.

## Visualizing the Call Stack

10. As a student, I want to be able to view the current stack of instructions, so that I can better understand what my program is doing.
11. As a student, when I get a type error (“head(xs) expects a pair, but encountered …”, division by zero), I want to be able to know which line it originated from, to find the problem faster.
12. As a student, when I get a type error (“head(xs) expects a pair, but encountered …”, division by zero), I want to be able to view the stack at the point the error takes place, to understand how the error came about.
13. As a student, I want my program to stop automatically if it looks like it has entered an infinite loop, then return a log of the current variable bindings and the history of the call stack, to avoid the browser crashing.

## Misc

14. Catch Infinite Loop (detect excessive function calls)